import React from "react";
import { Route, Routes } from "react-router-dom";
import { fullViewRoutes, routes } from "./routes";
import MainLayout from "layout/MainLayout";
import Loader from "components/Loader";

const Login = React.lazy(() => import("views/Login"));
const NotFound = React.lazy(() => import("views/NotFound"));
const ExamStart = React.lazy(() => import("views/Exam/ExamStart"));

import "./App.scss";
import 'react-h5-audio-player/lib/styles.css';
import FullView from "layout/FullView";

const App: React.FC = () => {
  return (
    <React.Suspense fallback={<Loader />}>
      <Routes>
        <Route
          path="*"
          element={
            <MainLayout>
              <Routes>
                {routes.map((route, index) => {
                  let Component = route.component;
                  return (
                    <Route
                      path={route.path}
                      element={<Component />}
                      key={index}
                    />
                  );
                })}
              </Routes>
            </MainLayout>
          }
        ></Route>
        <Route
          path="/exam/*"
          element={
            <FullView>
              <Routes>
                {fullViewRoutes.map((route, index) => {
                  let Component = route.component;
                  return (
                    <Route
                      path={route.path}
                      element={<Component />}
                      key={index}
                    />
                  );
                })}
              </Routes>
            </FullView>
          }
        ></Route>
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </React.Suspense>
  );
};

export default App;
