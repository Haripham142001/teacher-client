import { Input, Select, Switch } from "antd";
import PAGE_EDITOR from "constant/pageEditor";
import React from "react";
import { useSelector } from "react-redux";
import helper from "services/helper";
// import { FormItem, PageInfoField, TypeFormItem } from "services/model";
import { changeField, selectPageEditor } from "store/slices/pageEditorSlice";
import { useAppDispatch } from "store/store";
import OptionSelect from "./components/OptionSelect";

// type Props = {
//   data: FormItem;
//   tab: PageInfoField;
//   tabIndex: number;
// };

const EditField = (props) => {
  const dispatch = useAppDispatch();
  const pageEditor = useSelector(selectPageEditor);
  let configEditField = PAGE_EDITOR?.tabs?.[props.tab] || [];

  const handleChangeField = (field, value) => {
    dispatch(
      changeField({
        schema: props.tab,
        value,
        index: props.tabIndex,
        field,
      })
    );
  };

  const calcComponent = (fg) => {
    switch (fg.type) {
      case "text":
        return (
          <Input
            onChange={(e) => handleChangeField(fg.field, e.target.value)}
            value={props?.data?.[fg.field] || ""}
          />
        );
      case "select":
        let options = fg.options;
        if (fg?.store) {
          options = helper.convertArrayToSelect(
            pageEditor[fg.store],
            "name",
            "field"
          );
          options.unshift({
            label: "Please choose",
            value: "",
          });
        }
        return (
          <Select
            options={options}
            className="w-full"
            onChange={(e) => {
              handleChangeField(fg.field, e);
            }}
            value={props?.data?.[fg.field] || options?.[0]?.value}
          />
        );
      case "yesno":
        return (
          <Switch
            onChange={(e) => handleChangeField(fg.field, e)}
            checked={props?.data?.[fg.field] || false}
          />
        );
      case "relative":
        let opt = fg?.choose?.[props.data[fg.fieldRelative]]?.map((f) => ({
          label: f,
          value: f,
        }));
        return (
          <Select
            options={opt || [{ label: "Please choose", value: "" }]}
            value={props?.data?.[fg.field] || opt?.[0].value}
            className="w-full"
            onChange={(e) => handleChangeField(fg.field, e)}
          />
        );
      case "optionsSelect":
        return (
          <OptionSelect
            value={props?.data?.[fg.field] || []}
            onChange={(e) => handleChangeField(fg.field, e)}
          />
        );
      default:
        break;
    }
  };

  return (
    <div className="fieldgenerate">
      {configEditField?.map((fg, index) => {
        return (
          <div
            className="grid grid-cols-12 gap-4 border h-max p-1 hover:bg-gray-100"
            key={index}
          >
            <div
              className="col-span-5 font-medium"
              style={{ color: fg?.color ? fg.color : "black" }}
            >
              <span
                style={fg?.require && { color: "#39f", fontWeight: "bold" }}
              >
                {fg.label}
              </span>
            </div>
            <div className="col-span-7">{calcComponent(fg)}</div>
          </div>
        );
      })}
    </div>
  );
};

export default EditField;
