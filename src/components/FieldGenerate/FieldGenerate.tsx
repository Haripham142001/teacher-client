import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import Button from "antd-button-color";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { PageInfo, PageInfoField } from "services/model";
import {
  addItem,
  deleteItem,
  selectPageEditor,
} from "store/slices/pageEditorSlice";
import { useAppDispatch } from "store/store";
import EditField from "./EditField";

import "./fieldGenerate.scss";
import helper from "services/helper";

type Props = {
  name: string;
  description: string;
  tab: PageInfoField;
};

const FieldGenerate = (props: Props) => {
  const pageEditor: PageInfo = useSelector(selectPageEditor);
  const dispatch = useAppDispatch();
  let data = pageEditor[props.tab];

  const [activeTab, setActiveTab] = useState<number>(data.length > 0 ? 0 : -1);

  const handleAddFields = () => {
    if (checkFullField(props.tab)) {
      setActiveTab(data.length);
      dispatch(
        addItem({
          schema: props.tab,
          data: {
            name: "Field",
          },
        })
      );
    } else {
      return helper.toast("error", "Missing data");
    }
  };

  const handleDeleteItem = (index: number) => {
    dispatch(
      deleteItem({
        schema: props.tab,
        data: index,
      })
    );
  };

  const checkFullField = (tab: string) => {
    let { buttons, form, grid, apis } = pageEditor;
    switch (tab) {
      case "form":
        for (let f of form) {
          if (!f.name) return false;
          if (!f.field) return false;
          if (!f.type) return false;
        }
        return true;
      case "buttons":
        for (let button of buttons) {
          if (!button.name) false;
          if (!button.field) false;
          if (!button.active) false;
          if (!button.buttonType) false;
        }
        return true;
      case "grid":
        for (let g of grid) {
          if (!g.name) false;
          if (!g.field) false;
          if (!g.type) false;
        }
        return true;
      case "apis":
        for (let api of apis) {
          if (!api.name) false;
          if (!api.method) false;
          if (!api.field) false;
          if (!api.url) false;
        }
        return true;
      default:
        break;
    }
  };

  return (
    <div className="mt-2">
      <b>{props.name}</b>
      <p className="text-xs">{props.description}</p>
      <p className="text-right font-bold text-red-500">
        Attention: Blue field is require
      </p>
      <div className="grid grid-cols-12 gap-4 mt-3">
        <div className="col-span-3">
          <Button
            icon={<PlusOutlined />}
            onClick={handleAddFields}
            className="mb-2"
            type="primary"
          >
            <span>Thêm giá trị</span>
          </Button>
          {data?.map((dt, index) => {
            return (
              <div className="relative" key={index}>
                <button
                  onClick={() => setActiveTab(index)}
                  className={`add_field z-0 ${
                    index === activeTab ? "active" : ""
                  } border w-full h-max mb-1 text-left pl-3 py-1 rounded-md text-gray-500 hover:text-blue-500 hover:border-blue-500`}
                >
                  <p>
                    {dt?.name?.length > 16
                      ? dt.name.substring(0, 16) + "..."
                      : dt.name}
                  </p>
                  <p>
                    {dt?.field?.length > 16
                      ? dt.field.substring(0, 16) + "..."
                      : dt.field}
                  </p>
                </button>
                <div
                  className="absolute top-0 z-10 right-2 cursor-pointer hover:text-red-500"
                  onClick={() => handleDeleteItem(index)}
                >
                  <DeleteOutlined />
                </div>
              </div>
            );
          })}
        </div>
        <div className="col-span-9">
          {data?.[activeTab] && (
            <EditField
              data={data[activeTab]}
              tab={props.tab}
              tabIndex={activeTab}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default FieldGenerate;
