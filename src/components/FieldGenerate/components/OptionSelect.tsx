import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { Input } from "antd";
import React from "react";

type Option = { label: string; value: string };

type Props = {
  value: Option[];
  onChange: (data: any) => void;
};

type FieldOption = "label" | "value";

const OptionSelect = (props: Props) => {
  const handleChange = (index?: number) => {
    let _value: Option[] = [...props.value];
    if (!index) {
      _value.push({
        label: "",
        value: "",
      });
    } else {
      _value.splice(index, 1);
    }
    props.onChange(_value);
  };

  const handleChangeOption = (
    value: string,
    field: FieldOption,
    index: number
  ) => {
    let _value: Option[] = props.value.map((x) => x);
    _value[index] = {
        ..._value[index],
        [field]: value
    };
    props.onChange(_value);
  };

  return (
    <div className="h-max">
      <button
        className="border border-gray-300 h-8 px-2 mb-2 flex justify-center items-center hover:bg-blue-100"
        onClick={() => handleChange()}
      >
        <PlusOutlined />
        <span>Add option</span>
      </button>
      {props?.value?.map((val, index) => {
        return (
          <div className="grid grid-cols-11 gap-1 mb-1" key={index}>
            <Input
              className="col-span-5"
              placeholder="label"
              value={val?.label}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleChangeOption(e.currentTarget.value, "label", index)
              }
            />
            <Input
              className="col-span-5"
              placeholder="value"
              value={val?.value}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleChangeOption(e.currentTarget.value, "value", index)
              }
            />
            <button
              className="border border-gray-300 h-8 flex justify-center items-center rounded-md hover:bg-red-500 hover:text-white"
              onClick={() => handleChange(index)}
            >
              <DeleteOutlined />
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default OptionSelect;
