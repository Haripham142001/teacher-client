import { Select } from "antd";
import {
  WInput,
  TableInput,
  WSelectFromApi,
  WYesNo,
  WSelect,
  WDateTime,
  WMultiSelectAPI,
} from "components/widgets";
import React from "react";
import { ApiItem, FormItem } from "services/model";

type Props = {
  form: FormItem[];
  dataEmbedded?: string;
  data?: any;
  onChange: (state: any) => void;
  apis: ApiItem[];
};

const Default = (props: Props) => {
  return (
    <div className="grid grid-cols-12 gap-4">
      {props?.form?.map((f, index) => {
        if (f.type === "json" && !f?.interface) {
          f.interface = "tableInput";
        }
        if (f.type === "string" && !f?.interface) {
          f.interface = "text";
        }
        // if (f.type === "array" && !f.interface) {
        //   f.interface = "selectFromApi";
        // }
        if (f.type === "boolean" && !f.interface) {
          f.interface = "yesno";
        }
        switch (f?.interface) {
          case "multiSelectAPI":
            return (
              <WMultiSelectAPI
                options={f?.options || []}
                fields={f}
                key={index}
                value={props.data?.[f.field] || []}
                onChange={props.onChange}
                apis={props.apis}
              />
            );
          case "select":
            return (
              <WSelect
                options={f?.options || []}
                fields={f}
                key={index}
                value={props.data?.[f.field] || {}}
                onChange={props.onChange}
              />
            );
          case "tableInput":
            return (
              <TableInput
                key={index}
                fields={f}
                value={props.data?.[f.field] || []}
                onChange={props.onChange}
              />
            );
          case "selectFromApi":
            return (
              <WSelectFromApi
                key={index}
                fields={f}
                value={props.data?.[f.field] || []}
                onChange={props.onChange}
                apis={props.apis}
              />
            );
          case "yesno":
            return (
              <WYesNo
                key={index}
                fields={f}
                value={props.data?.[f.field] || false}
                onChange={props.onChange}
              />
            );
          case "date":
            return (
              <WDateTime
                key={index}
                fields={f}
                value={props.data?.[f.field]}
                onChange={props.onChange}
              />
            );
          default:
            return (
              <WInput
                key={index}
                fields={f}
                value={props.data?.[f.field]}
                onChange={props.onChange}
              />
            );
        }
      })}
    </div>
  );
};

export default Default;
