import { Modal } from "antd";
import React from "react";
import { ApiItem, FormItem } from "services/model";
import Default from "./Default";

type Props = {
  btn: any;
  onCancleClick: () => void;
  onOKClick: (data: any) => void;
  form?: FormItem[];
  data?: any;
  onChange: (state: any) => void;
  open: boolean;
  apis: ApiItem[];
};

const FormControl = (props: Props) => {
  let ModalDisplay;

  switch (props?.btn?.modal) {
    default:
      ModalDisplay = (
        <Default
          form={props?.form || []}
          dataEmbedded={props?.btn?.dataEmbedded}
          data={props?.data}
          onChange={props.onChange}
          apis={props.apis}
        />
      );
      break;
  }

  return (
    <Modal
      open={props.open}
      title={props?.btn?.name}
      onCancel={props.onCancleClick}
      onOk={async () => {
        let _data: {
          [field: string]: any;
        } = {};
        props?.form?.forEach((fo) => {
          if (fo.interface === "date" && !props.data[fo.field]) {
            const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = String(currentDate.getMonth() + 1).padStart(2, "0");
            const day = String(currentDate.getDate()).padStart(2, "0");
            _data[fo.field] = `${year}-${month}-${day}`;
          }
        });
        props.onOKClick(_data);
      }}
      width={Number(props?.btn?.size) || 400}
    >
      {props?.btn?.confirm && <span>{props?.btn?.confirm}</span>}
      {!props?.btn?.confirm && ModalDisplay}
    </Modal>
  );
};

export default FormControl;
