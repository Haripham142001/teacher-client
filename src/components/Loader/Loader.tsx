import React from "react";

import "./loader.scss";

type Props = {};

const Loader = (props: Props) => {
  return (
    <div className="loader">
      <div className="flexbox">
        <div className="nb-spinner"></div>
      </div>
    </div>
  );
};

export default Loader;
