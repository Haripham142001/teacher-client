import React from "react";
import { IconType } from "react-icons";
import * as icons from "@ant-design/icons";

type AntIconType = typeof icons;
type Props = {
  type: keyof AntIconType;
};

const RenderIcon = (props: Props) => {
  const Icon:any = icons[props?.type];
  return <Icon />;
};

export default RenderIcon;
