import { ArrowLeftOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

type Props = {};

const BackButton = (props: Props) => {
  const naviagate = useNavigate();
  return (
    <button
      onClick={() => naviagate(-1)}
      className="ml-1 px-4 rounded-md text-sm font-normal flex items-center bg-red-400 text-white hover:bg-red-500"
    >
      <ArrowLeftOutlined className="mr-1" />
      Quay lại
    </button>
  );
};

export default BackButton;
