import React, { useState } from "react";
import { Upload, UploadProps } from "antd";
import Button from "antd-button-color";
import { UploadOutlined } from "@ant-design/icons";
import { DataResponse } from "services/model";
import apiRequest from "services/api";
import local from "services/local";
import { HOST } from "constant/global";
import helper from "services/helper";

type Props = {
  onChange?: (data: any) => any;
  value?: any;
  type?: string;
};

const FileUploader = (props: Props) => {
  const [file, setFile] = useState<File | null>(null);

  const propsUpload: UploadProps = {
    name: "file",
    action: `${HOST}/others/uploadFile`,
    headers: {
      aAuthorization: "Bearer " + local.get("token", ""),
    },
    accept: props?.type || "image/*",
    onChange(info) {
      if (info.file?.response?.errorCode === 0) {
        props?.onChange?.(info.file.response.data);
      }
    },
  };

  if (props?.value) {
    propsUpload.fileList = [
      {
        uid: helper.generateDigitCode(16),
        name: props?.value.split("/upload/")[1],
        status: "done",
        url: props?.value,
      },
    ];
  }

  return (
    <Upload {...propsUpload}>
      <Button icon={<UploadOutlined />} disabled={file !== null} type="primary">
        Tải lên tệp
      </Button>
    </Upload>
  );
};

export default FileUploader;
