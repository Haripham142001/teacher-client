import { PlusOutlined } from "@ant-design/icons";
import { Button, Input, Select } from "antd";
import React from "react";
import { FormItem } from "services/model";

type Props = {
  fields: FormItem;
  value?: any;
  onChange: (state: any) => void;
};

const TableInput = (props: Props) => {
  const handleChangeSub = (field: string, value: any, index: number) => {
    let _value = [...props?.value];
    _value[index][field] = value;
    props.onChange({
      [props.fields.field]: _value,
    });
  };

  console.log(props.value);

  return (
    <div className="col-span-12">
      <p>{props.fields.name}</p>
      <button
        className="border mt-3 px-3 py-1 rounded flex items-center border-green-500 text-green-500 hover:bg-green-500 hover:text-white"
        onClick={() => {
          let _value = props.value;
          _value.push({});
          props.onChange({
            [props.fields.field]: _value,
          });
        }}
      >
        <PlusOutlined className="mr-1" /> Add field
      </button>
      <table className="mt-2 w-full">
        <thead>
          <tr>
            <th className="w-1/4">Name</th>
            <th className="w-1/4">Type</th>
            <th className="w-1/2">Value</th>
          </tr>
        </thead>
        <tbody>
          {props?.value?.map((val: any, index: number) => {
            return (
              <tr key={index} className="h-10">
                <td>
                  <Input
                    value={val?.name}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      handleChangeSub("name", e.currentTarget.value, index);
                    }}
                  />
                </td>
                <td className="pl-2">
                  <Select
                    options={OPTIONS_TYPE}
                    className="w-full"
                    value={val?.type}
                    onChange={(e) => {
                      handleChangeSub("type", e, index);
                    }}
                  />
                </td>
                <td className="pl-2">
                  <Input
                    value={val?.value}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      handleChangeSub("value", e.currentTarget.value, index);
                    }}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default TableInput;

const OPTIONS_TYPE = [
  {
    label: "Please choose",
    value: "",
  },
  {
    label: "string",
    value: "string",
  },
  {
    label: "number",
    value: "number",
  },
];
