import { DatePicker, DatePickerProps } from "antd";
import dayjs, { Dayjs } from "dayjs";
import React from "react";
import { FormItem } from "services/model";

type Props = {
  fields: FormItem;
  value?: any;
  onChange: (state: any) => void;
};

const WDateTime = (props: Props) => {
  const selectedDate: Dayjs | null = dayjs(props?.value || new Date());
  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    props.onChange({
      [props.fields.field]: dateString,
    });
  };
  const disabledDate = (current: any) => {
    // Disable dates that are before today
    return current && current < dayjs().startOf("day");
  };
  return (
    <div className="col-span-6">
      <p className="mb-2">
        {props.fields.name}
        <span className="text-red-500 font-bold">
          {props.fields?.required === true && " *"}
        </span>
      </p>
      <DatePicker
        className="w-full"
        onChange={onChange}
        value={selectedDate}
        disabledDate={disabledDate}
      />
    </div>
  );
};

export default WDateTime;
