import React, { useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

type Props = {
  value: string;
  onChange: (data: any) => void;
};

const WEditor = (props: Props) => {
  return (
    <ReactQuill
      theme="snow"
      value={props.value}
      onChange={(e) => props.onChange(e)}
    />
  );
};

export default WEditor;
