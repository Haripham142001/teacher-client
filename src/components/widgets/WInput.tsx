import { Input, InputNumber } from "antd";
import React, { HTMLInputTypeAttribute } from "react";
import { FormItem } from "services/model";

type Props = {
  fields: FormItem;
  value?: string | number;
  onChange: (state: any) => void;
};

const WInput = (props: Props) => {
  let Com;
  switch (props?.fields?.interface) {
    case "textArea":
      Com = (
        <Input.TextArea
          value={props?.value}
          onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
            props.onChange({
              [props.fields.field]: e.currentTarget.value,
            });
          }}
        />
      );
      break;
    case "number":
      Com = (
        <InputNumber
          className="w-full"
          value={Number(props?.value || 0)}
          onChange={(e: any) => {
            props.onChange({
              [props.fields.field]: e,
            });
          }}
        />
      );
      break;
    default:
      Com = (
        <Input
          value={props?.value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            props.onChange({
              [props.fields.field]: e.currentTarget.value,
            });
          }}
        />
      );
      break;
  }
  return (
    <div className="col-span-6">
      <p className="mb-2">
        {props.fields.name}
        <span className="text-red-500 font-bold">
          {props.fields?.required === true && " *"}
        </span>
      </p>
      {Com}
    </div>
  );
};

export default WInput;
