import { Select } from "antd";
import React, { useState, useEffect } from "react";
import helper from "services/helper";
import { ApiItem, DataResponse, FormItem } from "services/model";
import request from "services/request";

type Props = {
  fields: FormItem;
  value: any;
  onChange: (data: any) => void;
  options: any[];
  apis: ApiItem[];
};

const WMultiSelectAPI = (props: Props) => {
  console.log(props);

  const [data, setData] = useState<{ label: string; value: string }[]>([]);
  useEffect(() => {
    let api = props?.apis?.find((ap) => ap.field === props.fields.apiGetData);
    if (api) {
      fetchData(api.url);
    }
  }, []);

  const fetchData = async (url: string) => {
    const res: DataResponse<any[]> = await request.post(url, {});
    if (res?.errorCode === 0) {
      let _data = helper.convertArrayToSelect(res.data, "name", "sid");
      _data.unshift({ label: "Please choose", value: "" });
      setData(_data);
    }
  };

  return (
    <div className="col-span-6">
      <p>
        {props.fields.name}
        <span className="text-red-500 font-bold">
          {props.fields?.required === true && " *"}
        </span>
      </p>
      <Select
        options={data}
        showSearch
        mode="multiple"
        optionFilterProp="label"
        className="w-full mt-2"
        onChange={(e) =>
          props.onChange({
            [props.fields.field]: e,
          })
        }
        value={props.value}
        defaultValue={""}
      />
    </div>
  );
};

export default WMultiSelectAPI;
