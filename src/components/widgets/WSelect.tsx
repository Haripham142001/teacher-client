import { Select, Switch } from "antd";
import React from "react";
import { FormItem } from "services/model";

type Props = {
  fields: FormItem;
  value: boolean;
  onChange: (data: any) => void;
  options: any[];
};

const WSelect = (props: Props) => {
  return (
    <div className="col-span-6">
      <p>
        {props.fields.name}
        <span className="text-red-500 font-bold">
          {props.fields.required && "*"}
        </span>
      </p>
      <Select
        showSearch
        optionFilterProp="label"
        options={props?.options || []}
        className="w-full mt-2"
        onChange={(e) =>
          props.onChange({
            [props.fields.field]: e,
          })
        }
        value={props?.value}
      />
    </div>
  );
};

export default WSelect;
