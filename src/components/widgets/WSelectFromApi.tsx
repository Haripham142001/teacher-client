import { Select } from "antd";
import React, { useEffect, useState } from "react";
import helper from "services/helper";
import { ApiItem, DataResponse, FormItem } from "services/model";
import request from "services/request";

type Props = {
  fields: FormItem;
  value: string;
  onChange: (data: any) => void;
  apis: ApiItem[];
};

const WSelectFromApi = (props: Props) => {
  const [data, setData] = useState<{ label: string; value: string }[]>([]);
  let api = props?.apis?.find((ap) => ap.field === props.fields.apiGetData);
  useEffect(() => {
    if (api) {
      fetchData(api.url);
    }
  }, [api]);

  const fetchData = async (url: string) => {
    console.log(props.fields);

    const res: DataResponse<any[]> = await request.post(url, {});
    if (res?.errorCode === 0) {
      let _data = helper.convertArrayToSelect(
        res.data,
        props.fields?.fieldDisplay ? props.fields?.fieldDisplay : "name",
        "sid"
      );
      _data.unshift({ label: "Please choose", value: "" });
      setData(_data);
    }
  };

  return (
    <div className="col-span-6">
      <p>
        {props.fields.name}
        <span className="text-red-500 font-bold">
          {props.fields?.required === true && " *"}
        </span>
      </p>
      <Select
        options={data}
        showSearch
        optionFilterProp="label"
        className="w-full mt-2"
        onChange={(e) =>
          props.onChange({
            [props.fields.field]: e,
          })
        }
        value={props.value}
        defaultValue={""}
      />
    </div>
  );
};

export default WSelectFromApi;
