import { Switch } from "antd";
import React from "react";
import { FormItem } from "services/model";

type Props = {
  fields: FormItem;
  value: boolean;
  onChange: (data: any) => void;
};

const WYesNo = (props: Props) => {
  return (
    <div className="col-span-6">
      <p>
        {props.fields.name}
        <span className="text-red-500 font-bold">
          {props.fields.required && "*"}
        </span>
      </p>
      <Switch
        checked={props?.value || false}
        onChange={(e) => {
          props.onChange({
            [props.fields.field]: e,
          });
        }}
      />
    </div>
  );
};

export default WYesNo;
