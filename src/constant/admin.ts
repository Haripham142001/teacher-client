import { TiHomeOutline, TiUserOutline, TiDocument } from "react-icons/ti";
import { HiArrowNarrowRight } from "react-icons/hi";
import { BsUiChecks } from "react-icons/bs";
import { BsFillMenuButtonWideFill } from "react-icons/bs";
import { AiOutlineProfile, AiOutlineSetting } from "react-icons/ai";

export interface MenuItem {
  content: string;
  isParent?: boolean;
  icon?: any;
  link: string;
  child?: MenuItem[];
  isChild?: boolean;
  isActive?: string;
}

interface Admin {
  menu: MenuItem[];
}

const ADMIN: Admin = {
  menu: [
    {
      content: "Dashboard",
      isParent: false,
      icon: TiHomeOutline,
      link: "/",
    },
    {
      content: "Config",
      isParent: false,
      icon: AiOutlineSetting,
      link: "/page?sid=0167571391242815",
      isChild: true,
    },
    {
      content: "Menu management",
      isParent: false,
      icon: BsFillMenuButtonWideFill,
      link: "/page?sid=0167533227309638",
      isChild: true,
    },
    {
      content: "Page management",
      isParent: false,
      icon: AiOutlineProfile,
      link: "/page?sid=0167418678336408",
      isChild: true,
    },
  ],
};

export default ADMIN;
