export const colors: string[] = [
  "success",
  "danger",
  "primary",
  "warning",
  "info",
];
export const colorsButton: string[] = [
  "primary",
  "success",
  "info",
  "warning",
  "dark",
  "lightdark",
];
export const colorsHex: {
  [field: string]: string;
} = {
  success: "#2eb85c",
  danger: "#e55353",
  primary: "#321fdb",
  info: "#39f",
  warning: "#f9b115",
};

const ACTIVE_BUTTON = [
  {
    label: "Vui lòng chọn",
    value: "",
  },
  {
    label: "Gọi API",
    value: "call",
  },
  {
    label: "Navigation",
    value: "navigation",
  },
  {
    label: "Chuyển hướng",
    value: "redirect",
  },
  {
    label: "Hiện Popup",
    value: "popup",
  },
  {
    label: "Gọi API no body",
    value: "report",
  },
];

const MODAL = [
  {
    label: "Default",
    value: "default",
  },
];

const SIZE_POPUP = [
  {
    label: "Vui lòng chọn",
    value: "",
  },
  {
    label: "Lớn",
    value: "800",
  },
  {
    label: "Trung bình",
    value: "600",
  },
  {
    label: "Nhỏ",
    value: "400",
  },
];

export interface Field {
  label: string;
  type: "text" | "select" | "yesno" | "relative" | "optionsSelect";
  field: string;
  options?: {
    label: string;
    value: string;
  }[];
  choose?: {
    string: string[];
    number: string[];
    boolean: string[];
    json: any[];
    array?: any[];
  };
  fieldRelative?: string;
  require?: boolean;
  store?: string;
}

export interface ButtonField {
  label: string;
  type: "text" | "select" | "yesno";
  field: string;
  options?: {
    label: string;
    value: string;
  }[];
  store?: string;
  color?: string;
  require?: boolean;
  size?: string;
}

interface PageEditor {
  tabs: {
    form: Field[];
    buttons: ButtonField[];
    grid: ButtonField[];
    apis: ButtonField[];
  };
}

const PAGE_EDITOR: PageEditor = {
  tabs: {
    form: [
      {
        label: "Tên trường",
        type: "text",
        field: "name",
        require: true,
      },
      {
        label: "Trường dữ liệu",
        type: "text",
        field: "field",
        require: true,
      },
      {
        label: "Kiểu dữ liệu",
        type: "select",
        field: "type",
        require: true,
        options: [
          {
            label: "Vui lòng chọn",
            value: "",
          },
          {
            label: "string",
            value: "string",
          },
          {
            label: "number",
            value: "number",
          },
          {
            label: "boolean",
            value: "boolean",
          },
          {
            label: "json",
            value: "json",
          },
          // {
          //   label: "array",
          //   value: "array"
          // }
        ],
      },
      {
        label: "Trường bắt buộc",
        type: "yesno",
        field: "required",
      },
      {
        label: "Kiểu hiện thị",
        type: "relative",
        field: "interface",
        fieldRelative: "type",
        choose: {
          string: [
            "text",
            "textArea",
            "password",
            "select",
            "selectFromApi",
            "date",
          ],
          number: ["number", "date"],
          boolean: ["yesno"],
          json: ["tableInput", "multiSelectAPI"],
          // array: []
        },
        require: true,
      },
      {
        label: "Giá trị mặc định",
        type: "text",
        field: "default",
      },
      {
        label: "Các lựa chọn",
        type: "optionsSelect",
        field: "options",
      },
      {
        label: "Api lấy lựa chọn",
        type: "select",
        field: "apiGetData",
        store: "apis",
      },
      {
        label: "Trường hiển thị trong api",
        type: "text",
        field: "fieldDisplay",
      }
    ],
    buttons: [
      {
        label: "Tên nút",
        type: "text",
        field: "name",
        require: true,
      },
      {
        label: "Trường dữ liệu",
        field: "field",
        type: "text",
        require: true,
      },
      {
        label: "Màu sắc",
        type: "select",
        field: "color",
        options: colorsButton.map((color) => ({ label: color, value: color })),
      },
      {
        label: "Icon",
        type: "text",
        field: "icon",
      },
      {
        label: "Hành động",
        type: "select",
        field: "active",
        require: true,
        options: ACTIVE_BUTTON,
      },
      {
        label: "Modal",
        type: "select",
        field: "modal",
        options: MODAL,
      },
      {
        label: "Kích thước hiện thị Popup",
        type: "select",
        field: "size",
        options: SIZE_POPUP,
      },
      {
        label: "Dữ liệu nhúng",
        type: "text",
        field: "dataEmbedded",
      },
      {
        label: "Nội dung xác nhận",
        type: "text",
        field: "confirm",
      },
      {
        label: "Chọn API",
        field: "api",
        type: "select",
        options: [],
        store: "apis",
      },
      {
        label: "Hiển thị",
        type: "yesno",
        field: "view",
      },
      {
        label: "Link chuyển hướng",
        type: "text",
        field: "navigation",
      },
      {
        label: "Loại nút",
        type: "select",
        field: "buttonType",
        require: true,
        options: [
          {
            label: "Vui lòng chọn",
            value: "",
          },
          {
            label: "Gửi đi",
            value: "submit",
          },
          {
            label: "Nút bấm",
            value: "button",
          },
          // {
          //   label: "Switch",
          //   value: "switch",
          // },
        ],
      },
      {
        label: "Trường ngoại",
        field: "ref",
        type: "text",
      },
      {
        label: "Giá trị ngoại",
        field: "refVal",
        type: "text",
      },
    ],
    grid: [
      {
        label: "Tên cột",
        field: "name",
        type: "text",
        require: true,
      },
      {
        label: "Trường dữ liệu",
        field: "field",
        type: "text",
        require: true,
      },
      {
        label: "Cố định bên trái",
        field: "fixed",
        type: "yesno",
      },
      {
        label: "Kiểu dữ liệu",
        field: "type",
        type: "select",
        require: true,
        options: [
          {
            label: "Vui lòng chọn",
            value: "",
          },
          {
            label: "string",
            value: "string",
          },
          {
            label: "number",
            value: "number",
          },
          {
            label: "boolean",
            value: "boolean",
          },
        ],
      },
      {
        label: "Cho phép lọc",
        field: "filter",
        type: "yesno",
      },
      {
        label: "Hiển thị màu",
        field: "badges",
        type: "yesno",
      },
      {
        label: "Kích thước",
        field: "width",
        type: "text",
      },
      {
        label: "Success",
        field: "success",
        type: "text",
        color: colorsHex.success,
      },
      {
        label: "Warning",
        field: "warning",
        type: "text",
        color: colorsHex.warning,
      },
      {
        label: "Danger",
        field: "danger",
        type: "text",
        color: colorsHex.danger,
      },
      {
        label: "Info",
        field: "info",
        type: "text",
        color: colorsHex.info,
      },
      {
        label: "Primary",
        field: "primary",
        type: "text",
        color: colorsHex.primary,
      },
    ],
    apis: [
      {
        label: "Tên API",
        field: "name",
        type: "text",
        require: true,
      },
      {
        label: "Trường dữ liệu",
        field: "field",
        type: "text",
        require: true,
      },
      {
        label: "URL",
        field: "url",
        type: "text",
        require: true,
      },
      {
        label: "Phương thức",
        field: "method",
        type: "select",
        require: true,
        options: [
          {
            label: "Please choose",
            value: "",
          },
          {
            label: "GET",
            value: "get",
          },
          {
            label: "POST",
            value: "post",
          },
          // {
          //   label: "DELETE",
          //   value: "DELETE",
          // },
          // {
          //   label: "PUT",
          //   value: "PUT",
          // },
          // {
          //   label: "PATCH",
          //   value: "PATCH",
          // },
        ],
      },
      {
        label: "Loại API",
        field: "type",
        type: "select",
        options: [
          {
            label: "Please choose",
            value: "",
          },
          {
            label: "Find",
            value: "find",
          },
          {
            label: "Update",
            value: "update",
          },
        ],
      },
    ],
  },
};

export default PAGE_EDITOR;
