import { CheckOutlined } from "@ant-design/icons";
import {
  Card,
  Checkbox,
  Col,
  Input,
  InputNumber,
  Row,
  Tabs,
  Transfer,
} from "antd";
import type { TabsProps } from "antd";
import Button from "antd-button-color";
import { BackButton } from "components/UI";
import React, { ReactNode, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import apiRequest from "services/api";
import helper from "services/helper";
import { ClassInfo, FieldAdvanceExamConfig, IExamConfig } from "services/model";
import { DataResponse } from "services/model";

type Props = {};

interface RecordType {
  key: string;
  title: string;
  description: string;
  chosen: boolean;
}

const ExamConfig = (props: Props) => {
  const location = useLocation();
  let pageId: string = location.search.split("?sid=")[1];
  const [data, setData] = useState<Partial<IExamConfig>>({});
  const [mockData, setMockData] = useState<RecordType[]>([]);
  const [targetKeys, setTargetKeys] = useState<string[]>([]);
  console.log(data);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    let res: [DataResponse<ClassInfo[]>, DataResponse<IExamConfig>] =
      await Promise.all([
        apiRequest.getAllClass(),
        apiRequest.getExamConfigBySid({ sid: pageId }),
      ]);
    if (res[0]?.errorCode === 0 && res[1]?.errorCode === 0) {
      const tempTargetKeys: string[] = [];
      const tempMockData: RecordType[] = [];
      let exConfig = res[1].data;
      res[0].data.forEach((dt) => {
        let temp = {
          key: dt.sid,
          title: dt.subject_name + "-" + dt.idLop,
          description: "Khoa " + dt.major_name,
          chosen: false,
        };
        if (exConfig.classes.includes(temp.key)) temp.chosen = true;
        tempMockData.push(temp);
        if (temp.chosen) {
          tempTargetKeys.push(temp.key);
        }
      });
      setTargetKeys(tempTargetKeys);
      setMockData(tempMockData);
      setData(exConfig);
    }
  };

  const setStateData = (newState: any) => {
    setData((preState: any) => ({ ...preState, ...newState }));
  };

  const handleChangeTransfer = (newTargetKeys: string[]) => {
    setTargetKeys(newTargetKeys);
    setStateData({
      classes: newTargetKeys,
    });
  };

  const handleSaveExamConfig = async () => {
    const res: DataResponse<any> = await apiRequest.updateExamConfig({
      ...data,
    });
    if (res.errorCode === 0) {
      return helper.toast("success", "Cập nhật thành công");
    }
  };

  const TABS: TabsProps["items"] = [
    {
      label: "Lớp học phần",
      key: "1",
      children: (
        <Transfer
          showSearch
          dataSource={mockData}
          render={(item) => item.title}
          targetKeys={targetKeys}
          onChange={handleChangeTransfer}
          titles={["Danh sách lớp học phần", "Danh sách lớp thi"]}
        />
      ),
    },
    {
      label: "Nâng cao",
      key: "2",
      children: (
        <Row gutter={16}>
          <Col span={12}>
            <label className="font-bold">Cấu hình</label>
            <br />
            {CHECKBOXS.map((chk, index) => {
              return (
                <Checkbox
                  className="mt-3 w-full"
                  key={index}
                  checked={data?.advance?.[chk?.field] || chk?.default || false}
                  onChange={(e) =>
                    setStateData({
                      advance: {
                        ...data.advance,
                        [chk.field]: e.target.checked,
                      },
                    })
                  }
                >
                  {chk.label}
                </Checkbox>
              );
            })}
          </Col>
          <Col span={8}>
            <label htmlFor="">Thời gian cho phép vào muộn (phút)</label>
            <InputNumber
              className="mt-2 w-full mb-2"
              value={data.advance?.timeAllowLate || data?.time || 0}
              onChange={(e) =>
                setStateData({
                  advance: {
                    ...data.advance,
                    timeAllowLate: e,
                  },
                })
              }
            />
            <label htmlFor="">Khoảng thời gian chấm điểm tự động (ngày)</label>
            <InputNumber
              className="mt-2 w-full"
              value={data.advance?.timeAutoCheckPoint || 0}
              onChange={(e) =>
                setStateData({
                  advance: {
                    ...data.advance,
                    timeAutoCheckPoint: e,
                  },
                })
              }
            />
          </Col>
        </Row>
      ),
    },
  ];

  return (
    <Card
      title={
        <div className="flex justify-between">
          <p>Cấu hình ca thi</p>
          <div className="flex">
            <Button type="success" onClick={handleSaveExamConfig}>
              <CheckOutlined />
              Lưu
            </Button>
            <BackButton />
          </div>
        </div>
      }
    >
      <p className="text-3xl">
        {data?.name}
        <span className="text-blue-400 font-bold">
          {" " + data?.subject?.name + "-" + data?.subject?.idMH}
        </span>
      </p>
      <Row gutter={16} className="mt-3"></Row>
      <Tabs type="card" items={TABS} className="mt-3" />
    </Card>
  );
};

export default ExamConfig;

const CHECKBOXS: {
  label: string;
  field: FieldAdvanceExamConfig;
  default?: any;
}[] = [
  {
    label: "Cho phép xem đáp án sau khi nộp",
    field: "allowViewResult",
    default: false,
  },
  {
    label: "Cho phép hiển thị đáp án sau khi chấm điểm",
    field: "allowViewResultWhenCompleted",
    default: false,
  },
  {
    label: "Tạo mật khẩu vào thi",
    field: "autoGeneratePass",
    default: false,
  },
  {
    label: "Cho phép xem điểm trong cùng lớp",
    field: "allowViewOthersPoint",
    default: false,
  },
  // {
  //   label: "Chấm điểm trắc nghiệm tự động",
  //   field: "allowAutoCheckPoint",
  //   default: false,
  // },
];
