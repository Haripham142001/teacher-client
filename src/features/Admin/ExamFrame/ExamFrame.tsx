import { CheckOutlined, DeleteOutlined } from "@ant-design/icons";
import {
  Card,
  Col,
  Input,
  InputNumber,
  Row,
  Select,
  Button as ButtonAnt,
} from "antd";
import Button from "antd-button-color";
import { BackButton } from "components/UI";
import React, { ReactNode, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import apiRequest from "services/api";
import helper from "services/helper";
import {
  DataResponse,
  IExamFrame,
  WareHouse,
  QuestionSettingExamFrame,
  Subject,
} from "services/model";

type Props = {};

const ExamFrame = (props: Props) => {
  const location = useLocation();
  let pageId: string = location.search.split("?sid=")[1];
  const [data, setData] = useState<Partial<IExamFrame>>({
    setting: {
      number: 40,
      questions: [],
      type: "",
      totalPoint: 0,
    },
  });
  const [warehouses, setWarehouses] = useState<WareHouse[]>([]);
  const [current, setCurrent] = useState<number>(-1);
  const [number, setNumber] = useState<any>({});

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    let res: [
      DataResponse<IExamFrame>,
      DataResponse<WareHouse[]>,
      DataResponse<any>
    ] = await Promise.all([
      apiRequest.getExamFrameBySid({ sid: pageId }),
      apiRequest.getAllWarehouse(),
      apiRequest.getNumberQuestion(),
    ]);
    if (res[0]?.errorCode === 0) {
      setData(res[0].data);
    }
    if (res[1]?.errorCode === 0) {
      setWarehouses(res[1].data);
    }
    if (res[2]?.errorCode === 0) {
      setNumber(res[2].data);
    }
  };

  const setStateData = (newState: any) => {
    setData((preState: any) => ({ ...preState, ...newState }));
  };

  console.log(number);
  

  const handleSaveExam = async () => {
    let checkExistsCount0 = data.setting?.questions?.find(
      (qs) => qs.count === 0
    );
    if (checkExistsCount0) {
      return helper.toast(
        "error",
        "Không được để giá trị bằng 0 ở số lượng từng kho câu hỏi"
      );
    }

    for (let que of data.setting?.questions || []) {
      if (
        que.count !==
        (que?.countEasy || 0) + (que?.countMedium || 0) + (que?.countDiff || 0)
      ) {
        return helper.toast(
          "error",
          `Ở ${que.type} có tổng số câu hỏi không khớp.`
        );
      }
    }

    const count = data.setting?.questions?.reduce((init, curr) => {
      return (init += curr?.count || 0);
    }, 0);
    if (count !== data.setting?.number) {
      return helper.toast(
        "error",
        "Tổng số câu hỏi của các loại câu hỏi phải bằng " + data.setting?.number
      );
    }
    const res: DataResponse<any> = await apiRequest.updateExamFrame({
      ...data,
    });
    if (res.errorCode === 0)
      return helper.toast("success", "Cập nhật thành công");
  };

  const handleAddQuestionSetting = () => {
    if (!data.setting?.type)
      return helper.toast("error", "Chưa chọn loại đề thi");
    let _question = [...(data?.setting?.questions || [])];
    let _number = data.setting?.number || 40;
    _question.forEach((qs) => {
      _number -= qs.number;
    });
    _question.push({ number: _number, type: "", totalPoint: 0, count: 1 });
    setStateData({
      setting: {
        ...data.setting,
        questions: _question,
      },
    });
    setCurrent((cur) => (cur += 1));
  };

  const handleChangeQuestion = (field: string, value: any, index: number) => {
    let _question = JSON.parse(JSON.stringify(data?.setting?.questions));
    _question[index] = {
      ..._question[index],
      [field]: value,
    };
    if (field === "type") {
      _question[index].count = warehouses.find(
        (wh) => wh.sid === value
      )?.number;
    }
    setStateData({
      setting: {
        ...data.setting,
        questions: _question,
      },
    });
  };

  const handleDeleteQuestion = (index: number) => {
    let _question = JSON.parse(JSON.stringify(data?.setting?.questions));
    _question.splice(index, 1);
    setStateData({
      setting: {
        ...data.setting,
        questions: _question,
      },
    });
  };

  let arr2 = data?.setting?.questions?.map((qs) => qs.type) || [];
  let dataCurrrent = data?.setting?.questions?.[current];

  return (
    <Card
      title={
        <div className="flex justify-between">
          <span>Cấu hình khung đề thi</span>
          <div className="flex">
            <Button type="success" onClick={handleSaveExam}>
              <CheckOutlined />
              Lưu
            </Button>
            <BackButton />
          </div>
        </div>
      }
    >
      <p className="text-2xl font-bold">
        <span>{data.name}</span>
        <span className="text-blue-400">
          {" " + data?.subject?.name + "-" + data?.subject?.idMH}
        </span>
      </p>
      <Row gutter={16} className="mt-3">
        <Col span={8}></Col>
      </Row>
      <div className="mt-3">
        <Row className="mt-3" gutter={20}>
          <Col span={6}>
            <label>
              Kho<span className="text-red-500 font-bold">*</span>
            </label>
            <Select
              placeholder="Please choose"
              className="w-full mb-3 mt-2"
              options={helper.convertArrayToSelect(
                warehouses.filter((wh) => wh.subject_id === data.subject?.sid),
                "name",
                "sid"
              )}
              value={data?.warehouse_id}
              onChange={(e) => {
                setStateData({
                  warehouse_id: e,
                });
              }}
            />
          </Col>
          <Col span={6}>
            <label>
              Loại đề thi<span className="text-red-500 font-bold">*</span>
            </label>
            <Select
              className="w-full mt-2 mb-3"
              placeholder="Please choose"
              options={[
                {
                  label: "Chỉ có trắc nghiệm",
                  value: "fullChoice",
                },
                {
                  label: "Chỉ có tự luận",
                  value: "fullEssay",
                },
                {
                  label: "Cả tự luận cả trắc nghiệm",
                  value: "both",
                },
              ]}
              value={data.setting?.type}
              onChange={(e) =>
                setStateData({
                  setting: {
                    ...data.setting,
                    type: e,
                    questions: [],
                  },
                })
              }
            />
          </Col>
          <Col span={6}>
            <label>
              Tổng điểm<span className="text-red-500 font-bold">*</span>
            </label>
            <InputNumber
              className="w-full mt-2 mb-3"
              placeholder="Điểm"
              value={data.setting?.totalPoint}
              onChange={(e) =>
                setStateData({
                  setting: {
                    ...data.setting,
                    totalPoint: e,
                  },
                })
              }
            />
          </Col>
          <Col span={6}>
            <label htmlFor="">
              Số câu hỏi<span className="text-red-500 font-bold">*</span>
            </label>
            <InputNumber
              className="w-full mt-2 mb-3"
              placeholder="Số lượng"
              value={data.setting?.number}
              onChange={(e) =>
                setStateData({
                  setting: {
                    ...data.setting,
                    number: e,
                  },
                })
              }
            />
          </Col>
        </Row>
        <label
          className="underline cursor-pointer hover:text-blue-400"
          onClick={handleAddQuestionSetting}
        >
          Thêm loại câu hỏi
        </label>
        <Row className="mt-3">
          <Col md={8} className="max-h-96 min-h-0 overflow-y-scroll navbar">
            <ul className="h-max ml-2 pb-2 rounded">
              {data.setting?.questions?.map((ans, index) => {
                return (
                  <li
                    className={`${
                      index === current && "bg-green-100"
                    } border mb-1 py-1 px-3 rounded-md cursor-pointer hover:bg-green-100`}
                    key={index}
                    onClick={() => setCurrent(index)}
                  >
                    {ans.type ? ans.type : "Loại câu hỏi"}
                  </li>
                );
              })}
            </ul>
          </Col>
          {data?.setting?.questions?.[current] && (
            <Col md={16} className="pl-8 group-question">
              <table className="w-full table-examframe">
                <thead>
                  <tr>
                    <th className="w-36"></th>
                    <th className="flex justify-end">
                      <Button
                        type="primary"
                        onClick={() => handleDeleteQuestion(current)}
                      >
                        <DeleteOutlined />
                        Xóa
                      </Button>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Loại câu hỏi</td>
                    <td>
                      <Select
                        className="w-full"
                        options={TYPE_QUESTION.filter((que) => {
                          if (data.setting?.type === "fullChoice") {
                            return que.label !== "Tự luận";
                          } else if (data.setting?.type === "fullEssay") {
                            return que.label === "Tự luận";
                          }
                          return que;
                        }).filter((que) => !arr2.includes(que.label))}
                        onChange={(e) => {
                          handleChangeQuestion("type", e, current);
                        }}
                        value={dataCurrrent?.type}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td>Tổng điểm</td>
                    <td>
                      <InputNumber
                        className="w-full"
                        onChange={(e) => {
                          handleChangeQuestion("totalPoint", e, current);
                        }}
                        value={dataCurrrent?.totalPoint}
                        min={0}
                        max={data.setting.totalPoint}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td>Tổng số câu</td>
                    <td>
                      <InputNumber
                        className="w-full"
                        onChange={(e) => {
                          handleChangeQuestion("count", e, current);
                        }}
                        value={dataCurrrent?.count}
                        min={0}
                        max={data.setting.number}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td>Số câu dễ</td>
                    <td>
                      <InputNumber
                        className="w-full"
                        onChange={(e) => {
                          handleChangeQuestion("countEasy", e, current);
                        }}
                        value={dataCurrrent?.countEasy}
                        min={0}
                        // max={dataCurrrent?.count}
                        max={number?.findEasy}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td>Số câu trung bình</td>
                    <td>
                      <InputNumber
                        className="w-full"
                        onChange={(e) => {
                          handleChangeQuestion("countMedium", e, current);
                        }}
                        value={dataCurrrent?.countMedium}
                        min={0}
                        max={number?.findMedium}
                        // max={dataCurrrent?.count}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td>Số câu khó</td>
                    <td>
                      <InputNumber
                        className="w-full"
                        onChange={(e) => {
                          handleChangeQuestion("countDiff", e, current);
                        }}
                        value={dataCurrrent?.countDiff}
                        min={0}
                        // max={dataCurrrent?.count}
                        max={number?.findDiff}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </Col>
          )}
        </Row>
      </div>
    </Card>
  );
};

export default ExamFrame;

const TYPE_QUESTION = [
  {
    label: "Một phương án",
    value: "Một phương án",
  },
  {
    label: "Nhiều phương án",
    value: "Nhiều phương án",
  },
  {
    label: "Tự luận",
    value: "Tự luận",
  },
  {
    label: "Nhóm câu hỏi",
    value: "Nhóm câu hỏi",
  },
];
