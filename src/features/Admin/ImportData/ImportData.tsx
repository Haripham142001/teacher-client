import { Badge, Card, List, Select } from "antd";
import React, { useEffect, useState, ReactNode } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import StudentConfig from "./components/StudentConfig";
import Button from "antd-button-color";
import { DownloadOutlined, WarningOutlined } from "@ant-design/icons";
import FileUploader from "components/UI/FileUploader";
import { useSelector } from "react-redux";
import { selectConfig } from "store/slices/configsSlice";
import { BackButton } from "components/UI";
import { DataResponse } from "services/model";
import apiRequest from "services/api";
import helper from "services/helper";

type Props = {};
type Allow_import = "listStudent" | "student";
type ImportData = {
  advanceData: any;
  type: Allow_import;
  title?: string;
  temp: string;
  child?: ReactNode;
  api: string;
};

const ALLOW_IMPORT = ["listStudent", "student"];

const ImportData = (props: Props) => {
  const configSelector = useSelector(selectConfig);
  const location = useLocation();
  const navigate = useNavigate();
  let url = new URL(window.location.href);
  let params = new URLSearchParams(url.search);
  let type: Allow_import = params.get("type") || "listStudent";

  const [file, setFile] = useState<any>(null);
  const [data, setData] = useState<any>(null);

  useEffect(() => {
    if (file?.secure_url) {
      validateData();
    }
    if (!type) {
      return navigate(-1);
    } else if (!ALLOW_IMPORT.includes(type)) {
      return navigate(-1);
    }
  }, [file?.secure_url, params.get("sid")]);

  const setStateData = (newState: any) => {
    setData((preState: any) => ({ ...preState, ...newState }));
  };

  const ObjectImportData: {
    [field in Allow_import]: ImportData;
  } = {
    listStudent: {
      advanceData: {
        classId: params.get("sid"),
      },
      type: "listStudent",
      title: "Danh sách sinh viên",
      temp: "TEMPLATE_IMPORT_LIST_STUDENT",
      api: "validateDataUpload",
    },
    student: {
      advanceData: data,
      type: "student",
      title: "Danh sách sinh viên",
      temp: "TEMPLATE_IMPORT_STUDENT",
      api: "importStudent",
      child: <StudentConfig onChange={(e) => setStateData(e)} data={data} />,
    },
  };

  const handleDowloadTemplate = () => {
    window.open(
      configSelector?.["TEMPLATE_HOST"]?.subConfigs?.[
        ObjectImportData[type].temp
      ]?.value
    );
  };

  const validateData = async () => {
    const res: DataResponse<any> = await apiRequest[ObjectImportData[type].api](
      {
        url: file?.secure_url,
        ...data,
        ...ObjectImportData[type].advanceData
      }
    );
    if (res.errorCode === 0) {
      return helper.toast("success", "Import thành công");
    }
  };

  let ImportChild = ObjectImportData[type]?.child;

  return (
    <Card
      title={
        <div className="flex justify-between h-8">
          <p>Import {ObjectImportData[type]?.title}</p>
          <BackButton />
        </div>
      }
    >
      <div className="flex">
        {ObjectImportData[type]?.child && (
          <div className="w-1/2 pr-4">{ImportChild}</div>
        )}
        <div className="w-1/2">
          <Button
            icon={<DownloadOutlined />}
            className="mb-3"
            type="success"
            onClick={handleDowloadTemplate}
          >
            Tải xuống Template
          </Button>
          <Badge.Ribbon text="Chú ý" color="red">
            <div className="border rounded-lg overflow-hidden mb-3">
              <List
                dataSource={[
                  "Vui lòng tải xuống Template để có định dạng dữ liệu chính xác.",
                  "Tối đa 100 bản ghi.",
                  "Chỉ hỗ trợ file định dạng xlsx.",
                ]}
                renderItem={(item: string) => (
                  <div className="outline-1 outline-gray-200 outline h-12 flex items-center pl-4 bg-yellow-100">
                    <WarningOutlined className="mr-2 text-yellow-500" />
                    <span>{item}</span>
                  </div>
                )}
              />
            </div>
          </Badge.Ribbon>
          <FileUploader
            onChange={(e: any) => {
              if (e.url) {
                setFile(e);
              }
            }}
            type=".xlsx"
          />
        </div>
      </div>
    </Card>
  );
};

export default ImportData;
