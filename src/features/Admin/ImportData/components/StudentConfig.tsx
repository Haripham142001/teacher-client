import { Col, Input, Row, Select } from "antd";
import React, { useEffect, useState } from "react";
import apiRequest from "services/api";
import helper from "services/helper";
import { DataResponse, Specialization } from "services/model";

type Props = {
  onChange: (data: any) => void;
  data: any;
};

const StudentConfig = (props: Props) => {
  const [data, setData] = useState<{
    spe: Specialization[];
  }>({
    spe: [],
  });
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const res: [DataResponse<Specialization[]>] = await Promise.all([
      apiRequest.getAllSpecialization(),
    ]);
    if (res[0]?.errorCode === 0) {
      setStateData({ spe: res[0].data });
    }
  };

  const setStateData = (newState: any) => {
    setData((preState: any) => ({ ...preState, ...newState }));
  };

  return (
    <div>
      <label>Chọn chuyên ngành</label>
      <br />
      <Select
        className="w-full mt-2"
        placeholder="Please choose"
        options={helper.convertArrayToSelect(data?.spe, "name", "sid")}
        onChange={(e) =>
          props.onChange({
            spe: e,
          })
        }
        value={props.data?.spe || null}
      />
    </div>
  );
};

export default StudentConfig;
