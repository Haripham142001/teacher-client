import React, { useState, useEffect } from "react";
import {
  Card,
  Col,
  Collapse,
  Dropdown,
  Input,
  InputNumber,
  Modal,
  Row,
  Table,
} from "antd";
import Button from "antd-button-color";
import type { ColumnsType, TableProps } from "antd/es/table";
import { Link, useLocation, useNavigate } from "react-router-dom";
import apiRequest from "services/api";
import {
  ButtonItem,
  ClassInfo,
  DataResponse,
  FormItem,
  GridItem,
  Method,
  PageInfo,
} from "services/model";
import request from "services/request";
import helper from "services/helper";
import FormControl from "components/FormControl";
import {
  FilterOutlined,
  SearchOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import PAGE_EDITOR, {
  ButtonField,
  colors,
  colorsHex,
} from "constant/pageEditor";
import RenderIcon from "components/RenderIcon";
import { BackButton } from "components/UI";

const { Panel } = Collapse;

interface Page {
  pagination: {
    current: number;
    pageSize: number;
  };
  count: number;
}

const user: any = helper.getUser();

const ListStudent: React.FC = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [pageConfig, setPageConfig] = useState<Page>({
    pagination: {
      current: 1,
      pageSize: 10,
    },
    count: 0,
  });
  const [pageInfo, setPageInfo] = useState<PageInfo>({
    name: "",
    apis: [],
    buttons: [],
    form: [],
    grid: [],
    client: "",
    firstApi: "",
    sid: "",
  });
  const [data, setData] = useState<any[]>([]);
  const [isShow, setIsShow] = useState<boolean>(false);
  const [componentModal, setComponentModal] = useState<any>({});
  const [stateDataForm, setStateDataForm] = useState<any>(null);
  const [filter, setFilter] = useState<any>({
    queryInput: {
      limit: 10,
      sort: "DESC",
      current: 1,
    },
  });
  const [classInfo, setClassInfo] = useState<ClassInfo>({
    idLop: "",
    major_id: "",
    manager: "",
    semester: "",
    sid: "",
    status: "",
    students: [],
    subject_id: "",
    year: "",
  });
  let pageId: string = location.search.split("?sid=")[1];

  useEffect(() => {
    if (!pageId) {
      return navigate("/");
    }
    getPageInfo(pageId);
  }, [pageId]);

  const setStateFilter = (newState: any) => {
    setFilter((preState: any) => ({ ...preState, ...newState }));
  };

  const getPageInfo = async (pageId: string) => {
    let res: DataResponse<PageInfo> = await apiRequest["getPageInfoBySid"]({
      sid: user.role === "admin" ? "0169928293289547" : "0171997621866239",
    });
    let firstApiRes: DataResponse<{
      list: any;
      class: ClassInfo;
    }> = await apiRequest[
      user.role === "admin"
        ? "getListStudentByClassId"
        : "getAllStudentByClassIdFromTeacher"
    ]({
      sid: pageId,
      filter,
    });
    if (firstApiRes.errorCode === 0) {
      setPageInfo(res.data);
      setData(firstApiRes.data.list);
      setClassInfo(firstApiRes.data.class);
      setPageConfig({ ...pageConfig, count: firstApiRes?.count || 0 });
    }
  };

  const renderBtnSubmit = () => {
    const listBtn = pageInfo?.buttons?.filter(
      (button) => button.buttonType === "submit" && button?.view === true
    );
    return (
      <div className="flex flex-row-reverse">
        <BackButton />
        {listBtn?.map((button, index) => {
          return (
            <Button
              key={index}
              size="middle"
              onClick={() =>
                btnOnclick(button.field, listBtn, classInfo, pageInfo?.form)
              }
              icon={<RenderIcon type={button?.icon || "LineOutlined"} />}
              className="ml-1"
              type="primary"
            >
              {button.name}
            </Button>
          );
        })}
      </div>
    );
  };

  const calcTitle = (g: GridItem, data: any) => {
    let _value: string | boolean = data[g.field];
    if (g.type === "boolean") {
      _value = _value?.toString();
    }
    let color: string = "";
    for (let _color of colors) {
      let _g: any = g;
      let c = _g?.[_color]?.split(",")?.includes(_value) || "";
      if (c) {
        color = _color;
      }
    }
    return (
      <span
        style={{ background: colorsHex[color] }}
        className="px-2 py-1 text-white rounded-xl"
      >
        {_value}
      </span>
    );
  };

  const calculateColumns = () => {
    let { grid, buttons, form } = pageInfo;
    let columns = [];
    columns.push({
      title: "#",
      dataIndex: "index",
      width: 100,
      fixed: "left",
    });
    columns.push({
      title: "ID",
      dataIndex: "sid",
      width: 160,
      fixed: "left",
    });
    for (let g of grid) {
      let col: any = {
        title: g.name,
        dataIndex: g.field,
        key: g.field,
        render: (value: any, data: any) => {
          if (g?.badges) {
            return calcTitle(g, data);
          }
          if (typeof value === "string" && value.length >= 50) {
            return value.substring(0, 50) + "...";
          }
          return value;
        },
      };
      if (g?.fixed) {
        col.fixed = "left";
      }
      if (g?.width) {
        col.width = Number(g.width);
      } else {
        // col.width = 100;
      }
      columns.push(col);
    }
    let findButton = buttons.filter(
      (btn) => btn.buttonType === "button" && btn?.view === true
    );
    if (findButton.length > 0) {
      columns.push({
        title: "Action",
        dataIndex: "sid",
        key: "ppppp",
        width: 120,
        fixed: "right",
        render: (value: any, data: any) => {
          if (findButton.length > 1) {
            return (
              <Dropdown
                menu={{
                  items: findButton.map((btn) => ({
                    label: (
                      <div className="flex items-center w-24 h-8">
                        <RenderIcon type={btn.icon || "DownCircleOutlined"} />
                        <span className="ml-2">{btn.name}</span>
                      </div>
                    ),
                    key: btn.field,
                  })),
                  onClick: (e) => {
                    btnOnclick(e.key, findButton, data, form);
                  },
                }}
              >
                <div className="flex items-center border-[1px] px-3 py-1 border-gray-300 rounded-lg cursor-pointer text-gray-500 hover:bg-slate-200">
                  <SettingOutlined className="scale-110" />
                  <span className="ml-2">Action</span>
                </div>
              </Dropdown>
            );
          } else if (findButton.length === 1) {
            return (
              <div
                className="flex items-center border-[1px] px-3 py-1 border-gray-300 rounded-lg cursor-pointer text-gray-500 hover:bg-slate-200"
                onClick={() =>
                  btnOnclick(findButton[0].field, findButton, data, form)
                }
              >
                <RenderIcon type={findButton[0].icon || "DownCircleOutlined"} />
                <span className="ml-2">{findButton[0].name}</span>
              </div>
            );
          }
        },
      });
    }
    return columns;
  };

  const btnOnclick = (
    key: string,
    listBtn: ButtonItem[],
    _data: any,
    form?: FormItem[]
  ) => {
    let btn = listBtn.find((b) => b.field === key);
    switch (btn?.active) {
      case "navigation":
        let nav = helper.replaceURL(btn?.navigation || "", _data);
        navigate(nav);
        break;
      case "call":
        setStateDataForm(_data);
        if (btn?.confirm) {
          setComponentModal({
            btn: btn,
            _data: _data,
          });
          setIsShow(true);
        } else {
          callApiButton(btn, form);
        }
        break;
      case "redirect":
        window.open(btn?.navigation || "https://google.com.vn", "_blank");
        break;
      case "popup":
        setStateDataForm(_data);
        setComponentModal({
          form: form,
          btn: btn,
          _data: _data,
          apis: pageInfo.apis,
        });
        setIsShow(true);
        break;
      default:
        break;
    }
  };

  const callApiButton = async (btn: ButtonItem, form?: FormItem[]) => {
    if (!form) form = [];
    for (let f of form) {
      if (f.required && !stateDataForm[f.field])
        return helper.toast("error", "Missing data in " + f.name);
    }
    let api = pageInfo.apis.find((a) => a.field === btn.api);
    let res: DataResponse<any> = await request.post(
      api?.url || "",
      stateDataForm
    );
    if (res.errorCode === 0) {
      setIsShow(false);
      setStateDataForm({});
      getPageInfo(pageId);
      return helper.toast("success", res?.errorMsg || "Successfully");
    }
  };

  const handleFilter = async (_filter: any) => {
    let firstApiRes: DataResponse<any[]> = await apiRequest[
      "getListStudentByClassId"
    ]({
      sid: pageId,
      filter,
    });
    if (firstApiRes.errorCode === 0) {
      setData(firstApiRes.data);
      setPageConfig({ ...pageConfig, count: firstApiRes?.count || 0 });
    }
  };

  const onChange: TableProps<DataType>["onChange"] = async (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    let firstApi = pageInfo.apis.find((api) => api.field === pageInfo.firstApi);
    let method: Method = firstApi?.method || "post";
    let filtera = Object.assign({}, filter);
    filtera.queryInput = {
      ...filter.queryInput,
      current: pagination.current,
    };
    let firstApiRes: DataResponse<any[]> = await request?.[method](
      firstApi?.url || "",
      { filter: filtera }
    );
    if (firstApiRes.errorCode === 0) {
      setData(firstApiRes.data);
      setPageConfig({ ...pageConfig, count: firstApiRes?.count || 0 });
      setFilter(filtera);
    }
  };

  return (
    <React.Fragment>
      <Card
        title={
          <div className="h-10 mb-2 mt-2 flex justify-between items-center">
            <div>
              <b>
                {pageInfo?.name +
                  classInfo?.idLop +
                  "_" +
                  classInfo?.subject_id}
              </b>
              <span> - ({pageConfig.count} bản ghi)</span>
            </div>
            <div>{renderBtnSubmit()}</div>
          </div>
        }
      >
        <FormControl
          open={isShow}
          btn={componentModal.btn}
          form={componentModal.form}
          onCancleClick={() => {
            setIsShow(false);
            setStateDataForm({});
          }}
          onOKClick={() => callApiButton(componentModal.btn, pageInfo.form)}
          data={stateDataForm}
          onChange={(newState: any) => {
            setStateDataForm((preState: any) => ({ ...preState, ...newState }));
            setIsShow(true);
          }}
          apis={componentModal.apis}
        />
        <Collapse size="small" className="mb-3">
          <Panel
            header={
              <div className="flex items-center">
                <FilterOutlined className="mr-2" />
                <p>Lọc</p>
              </div>
            }
            key="1"
          >
            <Row gutter={10}>
              {pageInfo.grid
                .filter((gr) => gr.filter)
                .map((gr, index) => {
                  return (
                    <Col span={12} key={index}>
                      <p className="mt-3">{gr.name}</p>
                      <Input
                        value={filter?.[gr.field]}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          setStateFilter({ [gr.field]: e.currentTarget.value })
                        }
                      />
                    </Col>
                  );
                })}
              <Col span={12}>
                <p className="mt-3">Số lượng</p>
                <InputNumber
                  className="w-full"
                  value={filter?.queryInput?.limit || 10}
                  onChange={(e) =>
                    setStateFilter({
                      queryInput: {
                        ...filter?.queryInput,
                        limit: e,
                      },
                    })
                  }
                />
              </Col>
            </Row>
            <div className="flex justify-end">
              <Button
                className="mt-3"
                icon={<SearchOutlined />}
                onClick={handleFilter}
                type="primary"
              >
                Lọc
              </Button>
            </div>
          </Panel>
        </Collapse>
        <Table
          columns={calculateColumns()}
          dataSource={data?.map((dt, index) => ({
            ...dt,
            index: index + 1,
            key: dt?.sid || index,
          }))}
          onChange={onChange}
          pagination={{
            pageSize: filter.queryInput.limit,
            total:
              Math.ceil(pageConfig.count / filter.queryInput.limit) *
              filter.queryInput.limit,
          }}
          scroll={{ y: 535, x: 1300 }}
        />
      </Card>
    </React.Fragment>
  );
};

export default ListStudent;

interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}

const columns: ColumnsType<DataType> = [
  {
    title: "Name",
    dataIndex: "name",
    // width: "30%",
    fixed: "left",
    sorter: (a, b) => {
      if (a.name > b.name) return 1;
      else if (a.name < b.name) return -1;
      else return 0;
    },
  },
  {
    title: "Age",
    dataIndex: "age",
    width: 100,
    sorter: (a, b) => a.age - b.age,
  },
  {
    title: "Address",
    dataIndex: "address",
    // width: "40%",
    sorter: (a, b) => {
      if (a.address > b.address) return 1;
      else if (a.address < b.address) return -1;
      else return 0;
    },
  },
  {
    title: "Action",
    dataIndex: "",
    key: "x",
    render: () => {
      return <a>Delete</a>;
    },
    fixed: "right",
  },
];
