import ImportData from "features/Admin/ImportData";
import { useLocation } from "react-router-dom";

const ImportListStudent = () => {
  const location = useLocation();
  let classId: string = location.search.split("?sid=")[1];

  return <ImportData />;
};

export default ImportListStudent;
