import { Card, Input, Select, Tabs } from "antd";
import Button from "antd-button-color";
import React, { useState, useEffect } from "react";
import type { TabsProps } from "antd";
import FieldGenerate from "components/FieldGenerate";
import { useSelector } from "react-redux";
import {
  selectPageEditor,
  setPageInfoStore,
} from "store/slices/pageEditorSlice";
import helper from "services/helper";
import apiRequest from "services/api";
import { useLocation, useNavigate } from "react-router-dom";
import { DataResponse, PageInfo } from "services/model";
import { useAppDispatch } from "store/store";
import { BackButton } from "components/UI";

type Props = {};

const PageEditor = (props: Props) => {
  const location = useLocation();
  const pageEditor = useSelector(selectPageEditor);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [pageInfo, setPageInfo] = useState<{
    name: string;
    client: string;
    api: string;
    sid?: string;
  }>({
    name: "",
    client: "",
    api: "",
  });
  const [mode, setMode] = useState<string>("create");
  let sid = location?.search?.split("?sid=")?.[1]?.split("&mode")[0];

  useEffect(() => {
    if (location?.search) {
      if (sid) {
        fetchPageInfo(sid);
      }
      let _mode = location.search.split("&mode=")[1];
      if (_mode === "edit") {
        setMode(_mode);
      }
    } else if (sid === undefined) {
      dispatch(
        setPageInfoStore({
          buttons: [],
          apis: [],
          form: [],
          grid: [],
        })
      );
    }
  }, [sid]);

  const fetchPageInfo = async (sid: string) => {
    let res: DataResponse<PageInfo> = await apiRequest.getPageInfoBySid({
      sid,
    });
    if (res?.errorCode === 0) {
      let { name, firstApi, client, apis, buttons, grid, form, sid } = res.data;
      setPageInfo({
        name: name || "",
        api: firstApi || "",
        client: client || "",
        sid: sid || "",
      });
      dispatch(
        setPageInfoStore({
          apis: apis,
          buttons: buttons,
          grid: grid,
          form: form,
        })
      );
    }
  };

  const setStatePageInfo = (newState: any) => {
    setPageInfo((preState) => ({ ...preState, ...newState }));
  };

  const TABS: TabsProps["items"] = [
    {
      key: "form",
      label: "Form",
      children: (
        <FieldGenerate
          name="Biểu mẫu"
          description="Quản lý biểu mẫu"
          tab={"form"}
        />
      ),
    },
    {
      key: "button",
      label: "Button",
      children: (
        <FieldGenerate
          name="Nút bấm"
          description="Quản lý các nút"
          tab={"buttons"}
        />
      ),
    },
    {
      key: "grid",
      label: "Grid",
      children: (
        <FieldGenerate name="Bảng" description="Hiển thị bảng" tab={"grid"} />
      ),
    },
    {
      key: "api",
      label: "Api",
      children: (
        <FieldGenerate name="Apis" description="Quản lý API" tab={"apis"} />
      ),
    },
  ];

  const handleAddNewPage = async () => {
    if (!pageInfo.name) return helper.toast("error", "Thiếu trường name");
    let { buttons, form, grid, apis } = pageEditor;
    for (let f of form) {
      if (!f.name) return alertError("tên trang", "Form");
      if (!f.field) return alertError("Trường dữ liệu", "Form");
      if (!f.type) return alertError("Kiểu dữ liệu", "Form");
    }
    for (let button of buttons) {
      if (!button.name) return alertError("tên nút", "Button");
      if (!button.field) return alertError("trường", "Button");
      if (!button.active) return alertError("Hành động", "Button");
      if (!button.buttonType) return alertError("Kiểu hiện thị", "Button");
    }
    for (let g of grid) {
      if (!g.name) return alertError("tên cột", "Grid");
      if (!g.field) return alertError("trường", "Grid");
      if (!g.type) return alertError("kiểu dữ liệu", "Grid");
    }
    for (let api of apis) {
      if (!api.name) return alertError("tên api", "Api");
      if (!api.method) return alertError("Phương thức", "Api");
      if (!api.field) return alertError("trường dữ liệu", "Api");
      if (!api.url) return alertError("url", "Api");
    }
    let res = await apiRequest[mode === "create" ? "createPage" : "updatePage"](
      {
        ...pageEditor,
        ...pageInfo,
      }
    );
    if (res.errorCode === 0) {
      if (mode === "create") {
        helper.toast("success", "Tạo trang thành công");
        navigate("page?sid=0167418678336408");
      } else {
        helper.toast("success", "Cập nhật trang thành công");
      }
    }
  };

  const alertError = (label: string, tab: string) =>
    helper.toast("error", `Thiếu trường ${label} in ${tab}`);

  return (
    <Card
      title={
        <div className="flex justify-between">
          <span>Cập nhật</span>
          <div className="flex">
            {mode === "create" ? (
              <Button onClick={handleAddNewPage} type='primary'>+ Tạo mới</Button>
            ) : (
              <Button onClick={handleAddNewPage} type='primary'>+ Cập nhật</Button>
            )}
            <BackButton />
          </div>
        </div>
      }
    >
      <div className="grid grid-cols-12 gap-4">
        <div className="col-span-3">
          <label>Tên trang</label>
          <Input
            value={pageInfo.name}
            onChange={(e: React.FormEvent<HTMLInputElement>) =>
              setStatePageInfo({ name: e.currentTarget.value })
            }
          />
        </div>
        <div className="col-span-3">
          <label>Người dùng</label> 
          <br />
          <Select
            className="w-full"
            options={CLIENT}
            onChange={(e) => {
              setStatePageInfo({ client: e });
            }}
            value={CLIENT?.find((client) => client.value === pageInfo?.client)}
          />
        </div>
        <div className="col-span-3">
          <label>Api</label>
          <Select
            className="w-full"
            options={helper.convertArrayToSelect(
              pageEditor.apis.filter((api) => api?.field),
              "name",
              "field"
            )}
            value={helper.convertItemToValueSelect(
              pageEditor?.apis?.find((api) => api.field === pageInfo?.api),
              "name",
              "field"
            )}
            onChange={(e) => [setStatePageInfo({ api: e })]}
          />
        </div>
      </div>
      <Tabs items={TABS} className="mt-3" />
    </Card>
  );
};

export default PageEditor;

const CLIENT = [
  { value: "admin", label: "Admin" },
  { value: "teacher", label: "Teacher" },
  { value: "student", label: "Student" },
];
