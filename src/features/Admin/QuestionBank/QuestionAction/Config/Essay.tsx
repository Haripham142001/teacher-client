import { Checkbox, Col, Row, Select, Space } from "antd";
import React from "react";
import { ConfigQuestion } from "services/model";

type Props = {
  data: ConfigQuestion;
  onChange: (data: any) => void;
};

const { Option } = Select;

const Essay = (props: Props) => {
  const handleChangeField = (field: string, value: any) => {
    let _data = Object.assign({}, props.data);
    _data.advance[field] = value;
    props.onChange(_data);
  };

  return (
    <>
      <Row gutter={16}>
        <Col span={8}>
          <label>Cho phép upload file</label>
          <Select
            mode="multiple"
            className="w-full my-2"
            placeholder="Please choose"
            onChange={(e) => {
              let _data = Object.assign({}, props.data);
              _data.advance.allowFile = e;
              props.onChange(_data);
            }}
            value={props.data?.advance?.allowFile || []}
          >
            <Option label="Excel" value="excel">
              <Space>Excel (.xlsx)</Space>
            </Option>
            <Option label="Word" value="word">
              <Space>Word (.docx)</Space>
            </Option>
            <Option label="JS" value="js">
              <Space>Javascript (.js)</Space>
            </Option>
            <Option label="HTML" value="html">
              <Space>HTML (.html)</Space>
            </Option>
            <Option label="CSS" value="css">
              <Space>CSS (.css)</Space>
            </Option>
            <Option label="Image" value="image">
              <Space>Image (.jpg)</Space>
            </Option>
          </Select>
        </Col>
        <Col span={8}>
          <p className="font-bold">Cấu hình</p>
          <Checkbox
            className="mt-2"
            onChange={(e) =>
              handleChangeField("allowContent", e.target.checked)
            }
            defaultChecked={props.data?.advance?.allowContent || false}
          >
            Cho phép nhập nội dung
          </Checkbox>
        </Col>
      </Row>
    </>
  );
};

export default Essay;
