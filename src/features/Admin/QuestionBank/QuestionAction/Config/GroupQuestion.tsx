import { PlusOutlined } from "@ant-design/icons";
import { Col, Input, Radio, Row } from "antd";
import Button from "antd-button-color";
import React, { useState } from "react";
import helper from "services/helper";
import { ConfigQuestion } from "services/model";

type Props = {
  data: ConfigQuestion;
  onChange: (data: any) => void;
  onChangeNumber: (data: number) => void;
};

const GroupQuestion = (props: Props) => {
  const [current, setCurrent] = useState<number>(0);

  const handleAddOptions = () => {
    let _data = Object.assign({}, props.data);
    _data.answers?.push({
      key: helper.generateDigitCode(6),
      label: "Câu " + ((props?.data?.answers?.length || 0) + 1),
      options: [],
      isGroup: true,
      correct: "",
    });
    props.onChange(_data);
    props.onChangeNumber(_data.answers?.length || 0);
  };

  const handleAddOption = () => {
    let _data = Object.assign({}, props.data);
    if (_data?.answers?.[current]) {
      _data?.answers?.[current].options?.push({
        label:
          "Đáp án " +
          ((props.data?.answers?.[current].options?.length || 0) + 1),
        key: helper.generateDigitCode(6),
      });
    }
    props.onChange(_data);
  };

  const handleDeleteOption = (index2: number) => {
    let _data = Object.assign({}, props.data);
    if (_data?.answers?.[current]) {
      _data?.answers?.[current].options?.splice(index2, 1);
    }
    props.onChange(_data);
    props.onChangeNumber(_data.answers?.length || 0);
  };

  const handleChangeOptionCorrect = (value: string) => {
    let _data = Object.assign({}, props.data);
    if (_data?.answers?.[current]) {
      _data.answers[current].correct = value;
    }
    props.onChange(_data);
  };

  const handleChangeDapAn = (value: string, index2: number) => {
    let _data = Object.assign({}, props.data);
    if (_data?.answers?.[current]?.options?.[index2]) {
      _data.answers[current].options[index2].label = value;
    }
    props.onChange(_data);
  };

  return (
    <div>
      <Button icon={<PlusOutlined />} type="primary" onClick={handleAddOptions}>
        Thêm câu hỏi
      </Button>
      <Row className="mt-3">
        <Col md={8} className="max-h-96 min-h-0 overflow-y-scroll navbar">
          <ul className="h-max ml-2 pb-2 rounded">
            {props.data.answers?.map((ans, index) => {
              return (
                <li
                  className={`${
                    index === current && "bg-green-100"
                  } border mb-1 py-1 px-3 rounded-md cursor-pointer hover:bg-green-100`}
                  key={index}
                  onClick={() => setCurrent(index)}
                >
                  {ans.label}
                </li>
              );
            })}
          </ul>
        </Col>
        <Col md={16} className="pl-8 group-question overflow-y-scroll">
          <div className="h-64">
            <Button
              // className="text-right cursor-pointer hover:underline"
              type="success"
              className="mb-3"
              onClick={handleAddOption}
            >
              Thêm đáp án
            </Button>
            {props.data?.answers?.[current] && (
              <Input.TextArea
                value={props.data?.answers?.[current]?.content || ""}
                onChange={(e) => {
                  let _data = Object.assign({}, props.data);
                  if (_data.answers?.[current])
                    _data.answers[current].content = e.target.value;
                  props.onChange(_data);
                }}
                placeholder="Nhập nội dung câu hỏi"
              />
            )}
            <Radio.Group
              options={
                props.data?.answers?.[current]?.options?.map((ans, index2) => ({
                  label: (
                    <div className="flex items-center">
                      <Input
                        value={ans.label}
                        className="my-2 w-full mr-2"
                        onChange={(e) =>
                          handleChangeDapAn(e.target.value, index2)
                        }
                      />
                      <Button
                        onClick={() => handleDeleteOption(index2)}
                        className="border border-red-400 text-red-500 hover:bg-red-500 hover:border-red-400 hover:text-white"
                      >
                        x
                      </Button>
                    </div>
                  ),
                  value: ans.key,
                })) || []
              }
              onChange={(e) => handleChangeOptionCorrect(e.target.value)}
              value={props?.data?.answers?.[current]?.correct || null}
            ></Radio.Group>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default GroupQuestion;
