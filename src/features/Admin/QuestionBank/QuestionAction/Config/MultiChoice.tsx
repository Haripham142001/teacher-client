import {
  CheckOutlined,
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { Checkbox, Input, Modal } from "antd";
import Button from "antd-button-color";
import React, { useState } from "react";
import helper from "services/helper";
import { ConfigQuestion } from "services/model";

type Props = {
  data: ConfigQuestion;
  onChange: (data: any) => void;
};

const MultiChoice = (props: Props) => {
  const [isShow, setIsShow] = useState(false);
  const [input, setInput] = useState<string>("");
  const [chk, setChk] = useState<boolean>(false);
  const [sidAns, setSidAns] = useState<string>("");
  const [isEdit, setIsEdit] = useState<boolean>(false);

  return (
    <>
      <Button
        size="middle"
        className="mb-3"
        onClick={() => {
          setInput("");
          setChk(false);
          setIsShow(true);
          setIsEdit(false);
        }}
        icon={<PlusOutlined />}
        type="primary"
      >
        Thêm đáp án
      </Button>
      <table className="w-full">
        <thead>
          <tr className="w-full">
            <th className="w-1/6">#</th>
            <th className="w-1/2">Đáp án</th>
            <th className="w-1/6">Đáp án đúng</th>
            <th className="w-1/6">Hành động</th>
          </tr>
        </thead>
        <tbody>
          {props?.data?.answers?.map((ans, index) => {
            return (
              <tr key={index} className="mb-3 h-12">
                <td className="text-center">{index + 1}</td>
                <td className={`${ans.isCorrect && "text-blue-500 font-bold"}`}>
                  {ans.label}
                </td>
                <td className="text-center">
                  {ans.isCorrect && (
                    <CheckOutlined className="text-green-500 scale-150" />
                  )}
                </td>
                <td className="text-center">
                  <EditOutlined
                    className="cursor-pointer hover:text-blue-500 scale-150 mr-3"
                    onClick={() => {
                      setInput(ans?.label || "");
                      setChk(ans?.isCorrect || false);
                      setSidAns(ans.key);
                      setIsEdit(true);
                      setIsShow(true);
                    }}
                  />
                  <DeleteOutlined
                    className="cursor-pointer hover:text-red-500 scale-150"
                    onClick={() => {
                      let _answers = [...(props?.data?.answers || [])];
                      _answers = _answers.filter((a) => a.key !== ans.key);
                      props.onChange({ answers: _answers });
                    }}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Modal
        open={isShow}
        title="Thêm đáp án"
        onCancel={() => {
          setInput("");
          setChk(false);
          setIsShow(false);
        }}
        onOk={() => {
          if (input === "") return helper.toast("error", "Thiếu dữ liệu");
          let _answers = [...(props?.data?.answers || [])];
          let sid = "";
          if (!isEdit) {
            sid = helper.generateDigitCode(5);
            _answers.push({
              label: input,
              key: sid,
              isCorrect: chk,
            });
          } else {
            let i: number = _answers.findIndex((ans) => ans.key === sidAns);
            _answers[i] = {
              key: sidAns,
              label: input,
              isCorrect: chk,
            };
          }
          setInput("");
          setChk(false);
          setIsShow(false);
          props.onChange({ answers: _answers });
        }}
      >
        <Input.TextArea
          className="mt-3"
          placeholder="Nhập đáp án"
          onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
            setInput(e.currentTarget.value)
          }
          value={input}
        />
        <Checkbox
          className="mt-3"
          onChange={(e) => setChk(e.target.checked)}
          checked={chk}
        >
          Đáp án đúng
        </Checkbox>
      </Modal>
    </>
  );
};

export default MultiChoice;
