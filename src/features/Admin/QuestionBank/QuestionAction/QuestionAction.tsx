import { Card, Col, Collapse, Input, InputNumber, Row, Select } from "antd";
import Button from "antd-button-color";
import React, { useState, useEffect } from "react";
import SingleChoice from "./Config/SingleChoice";
import {
  Answer,
  DataResponse,
  Question,
  Subject,
  WareHouse,
} from "services/model";
import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import apiRequest from "services/api";
import helper from "services/helper";
import { useNavigate, useLocation } from "react-router-dom";
import { ConfigQuestion } from "services/model";
import MultiChoice from "./Config/MultiChoice";
import GroupQuestion from "./Config/GroupQuestion";
import { BackButton } from "components/UI";
import Essay from "./Config/Essay";
import FileUploader from "components/UI/FileUploader";
import { WEditor } from "components/widgets";

const { Panel } = Collapse;

type Props = {};

interface ValidateResult {
  error: boolean;
  message: string;
}

const QuestionAction = (props: Props) => {
  const navigate = useNavigate();
  const location = useLocation();
  let sid = location?.search?.split("?sid=")?.[1]?.split("&mode")[0];
  const [data, setData] = useState<Question>({
    sid: "",
    content: "",
    number: 1,
    config: {
      answers: [],
      advance: {},
    },
    status: "pending",
    instruct: "",
    type: "",
    specialization_id: "",
    subject_id: "",
    warehouse_id: [],
    level: "",
  });
  const [warehouses, setWarehouses] = useState<WareHouse[]>([]);
  const [mode, setMode] = useState<"create" | "edit">("create");
  const [subjects, setSubjects] = useState<Subject[]>([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    if (sid) {
      let resData: DataResponse<Question> = await apiRequest.getQuestionBySid({
        sid,
      });
      if (resData.errorCode === 0) {
        setData(resData.data);
      }
      let _mode = location.search.split("&mode=")[1];
      if (_mode === "edit") {
        setMode(_mode);
      }
    }

    let res: [DataResponse<WareHouse[]>, DataResponse<Subject[]>] =
      await Promise.all([
        apiRequest.getAllWarehouseDefault({}),
        apiRequest.getAllSubject({}),
      ]);
    if (res[0].errorCode === 0) {
      setWarehouses(res[0].data);
    }
    if (res[1].errorCode === 0) {
      setSubjects(res[1].data);
    }
  };

  const setStateData = (newState: any) => {
    setData((preState) => ({ ...preState, ...newState }));
  };

  const handleAddQuestion = async () => {
    let { error, message }: ValidateResult = validateConfigQuestion(
      data.type,
      data.config
    );
    if (!error) {
      return helper.toast("error", message);
    }
    if (!data.type || !data.number || !data.warehouse_id || !data.content)
      return helper.toast("error", "Thiếu dữ liệu");
    let res: DataResponse<any> = await apiRequest.createQuestion({ ...data });
    if (res?.errorCode === 0) {
      helper.toast("success", "Thêm thành công");
      return navigate(-1);
    }
  };

  const handleUpdateQuestion = async () => {
    let { error, message }: ValidateResult = validateConfigQuestion(
      data.type,
      data.config
    );
    if (!error) {
      return helper.toast("error", message);
    }
    if (!data.type || !data.number || !data.warehouse_id || !data.content)
      return helper.toast("error", "Thiếu dữ liệu");
    let res: DataResponse<any> = await apiRequest.updateQuestion({ ...data });
    if (res?.errorCode === 0) {
      return helper.toast("success", "Cập nhật thành công");
    }
  };

  const validateConfigQuestion = (
    type: string,
    config: ConfigQuestion
  ): ValidateResult => {
    let error: boolean = true,
      message: string = "";
    switch (type) {
      case "Một phương án":
        if (config?.answers && config.answers.length <= 2) {
          error = false;
          message = "Số lượng phương án phải lớn hơn hoặc bằng 2";
        }
        let check = config?.answers?.find((cfg: Answer) => cfg.isCorrect);
        if (!check) {
          error = false;
          message = "Chưa chọn đáp án";
        }
        break;
      case "Nhóm câu hỏi":
        if (config?.answers && config.answers.length <= 1) {
          error = false;
          message = "Số lượng phương án phải lớn hơn hoặc bằng 1";
        }
        for (let ans of config?.answers || []) {
          if (!ans.correct) {
            error = false;
            message = "Một số câu hỏi chưa có đáp án";
          }
        }
        break;
      default:
        break;
    }
    return {
      error,
      message,
    };
  };

  let Config;
  switch (data?.type) {
    case "Một phương án":
      Config = (
        <SingleChoice
          data={data.config}
          onChange={(e) => {
            setStateData({ config: e });
          }}
        />
      );
      break;
    case "Nhiều phương án":
      Config = (
        <MultiChoice
          data={data.config}
          onChange={(e) => {
            setStateData({ config: e });
          }}
        />
      );
      break;
    case "Nhóm câu hỏi":
      Config = (
        <GroupQuestion
          data={data.config}
          onChange={(e) => {
            setStateData({ config: e });
          }}
          onChangeNumber={(e: number) => {
            setStateData({
              number: e,
            });
          }}
        />
      );
      break;
    case "Tự luận":
      Config = (
        <Essay
          data={data.config}
          onChange={(e) => {
            setStateData({ config: e });
          }}
        />
      );
      break;
    default:
      break;
  }

  return (
    <Card
      title={
        <div className="flex justify-between">
          <span>Tạo mới câu hỏi</span>
          <div className="flex">
            {mode === "create" ? (
              <Button onClick={handleAddQuestion} type="primary">
                <PlusOutlined />
                Tạo mới
              </Button>
            ) : (
              <Button onClick={handleUpdateQuestion} type="primary">
                <EditOutlined />
                Cập nhật
              </Button>
            )}
            <BackButton />
          </div>
        </div>
      }
    >
      <Row gutter={20}>
        <Col span={12}>
          <p>
            <b>Loại câu hỏi: </b>
          </p>
          <Select
            className="mt-3 w-full"
            options={OPTIONS_TYPE_QUESTION}
            placeholder="Vui lòng chọn"
            allowClear
            value={data?.type || null}
            onChange={(e) => {
              setStateData({
                type: e === undefined ? null : e,
              });
            }}
          />
        </Col>
        <Col span={6}>
          <p>
            <b>Số lượng câu chiếm trong đề thi: </b>
          </p>
          <InputNumber
            className="mt-3 w-full"
            value={data?.number}
            onChange={(e) => setStateData({ number: e })}
          />
        </Col>
        <Col span={6}>
          <p>
            <b>Độ khó: </b>
          </p>
          <Select
            className="mt-3 w-full"
            options={[
              {
                label: "Dễ",
                value: "Dễ",
              },
              {
                label: "Trung bình",
                value: "Trung bình",
              },
              {
                label: "Khó",
                value: "Khó",
              },
            ]}
            placeholder="Vui lòng chọn"
            allowClear
            value={data?.level || null}
            onChange={(e) => {
              setStateData({
                level: e === undefined ? null : e,
              });
            }}
          />
        </Col>
      </Row>
      <Row gutter={20} className="mt-4">
        <Col span={12}>
          <p>
            <b>Môn học: </b>
          </p>
          <Select
            className="mt-3 w-full"
            showSearch
            optionFilterProp="label"
            options={helper.convertArrayToSelect(subjects, "name", "sid")}
            placeholder="Vui lòng chọn"
            allowClear
            value={data?.subject_id}
            onChange={(e) => {
              setStateData({
                subject_id: e === undefined ? null : e,
                warehouse_id: [],
              });
            }}
          />
        </Col>
        <Col span={12}>
          <p>
            <b>Kho: </b>
          </p>
          <Select
            mode="multiple"
            className="mt-3 w-full"
            showSearch
            optionFilterProp="label"
            options={warehouses
              .filter((opt) => opt.subject_id === data.subject_id)
              .map((opt) => {
                return {
                  label: opt.name,
                  value: opt.sid,
                };
              })}
            placeholder="Vui lòng chọn"
            allowClear
            value={data?.warehouse_id || null}
            onChange={(e) => {
              setStateData({
                warehouse_id: e === undefined ? null : e,
              });
            }}
          />
        </Col>
      </Row>
      <Row gutter={[20, 16]} className="mt-4">
        <Col span={24}>
          <b>Nội dung câu hỏi: </b>
          {/* <Input.TextArea
            className="mt-3"
            rows={3}
            value={data?.content}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
              setStateData({ content: e.currentTarget.value })
            }
          /> */}
          <WEditor
            value={data?.content}
            onChange={(e: string) => setStateData({ content: e })}
          />
        </Col>
        <Col span={12}>
          <p>Hướng dẫn làm bài: </p>
          <Input.TextArea
            className="mt-3"
            rows={3}
            value={data?.instruct}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
              setStateData({ instruct: e.currentTarget.value })
            }
          />
        </Col>
        <Col span={6}>
          <b>Tệp đính kèm hình ảnh</b>
          <br />
          <FileUploader
            onChange={(e: any) => {
              if (e.url) {
                setStateData({ attachment: e.url });
              }
            }}
            value={data.attachment}
            type="image/*"
          />
        </Col>
        <Col span={6}>
          <b>Tệp đính kèm âm thanh</b>
          <br />
          <FileUploader
            onChange={(e: any) => {
              if (e.url) {
                setStateData({ sound: e.url });
              }
            }}
            value={data.sound}
            type="audio/*"
          />
        </Col>
      </Row>
      {data?.type !== "" && (
        <Row className="mt-4">
          <Collapse
            size="small"
            className="w-full mt-3 bg-gray-100"
            defaultActiveKey={1}
          >
            <Panel header={<b>Cấu hình</b>} key="1">
              {Config}
            </Panel>
          </Collapse>
        </Row>
      )}
    </Card>
  );
};

export default QuestionAction;

const OPTIONS_TYPE_QUESTION = [
  {
    label: "Một phương án",
    value: "Một phương án",
  },
  {
    label: "Nhiều phương án",
    value: "Nhiều phương án",
  },
  {
    label: "Tự luận",
    value: "Tự luận",
  },
  {
    label: "Nhóm câu hỏi",
    value: "Nhóm câu hỏi",
  },
];
