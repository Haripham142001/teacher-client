import { Card, Col, Drawer, Row, Select } from "antd";
import { BackButton } from "components/UI";
import React, { useState } from "react";
import moment from "moment";
import { ArrowLeftOutlined, ArrowRightOutlined } from "@ant-design/icons";

import "./schedule.scss";

type Props = {};

interface Activity {
  day: number;
  start: number;
  end: number;
  title: string;
  idMh?: string;
  manager?: string;
}

const Schedule = (props: Props) => {
  const [open, setOpen] = useState(false);
  const days = [
    "Thứ 2",
    "Thứ 3",
    "Thứ 4",
    "Thứ 5",
    "Thứ 6",
    "Thứ 7",
    "Chủ nhật",
  ];
  const periods = Array.from({ length: 11 }, (_, index) => index + 1);

  const activities: Activity[] = [
    {
      day: 5,
      start: 1,
      end: 6,
      title: "Trí tuệ nhân tạo + BTL",
      idMh: "7080211",
      manager: "Phạm Đức Hải",
    },
    { day: 1, start: 5, end: 8, title: "Phân tích thiết kế & hệ thống + BTL" },
    { day: 2, start: 2, end: 4, title: "Cơ sở lập trình" },
    { day: 2, start: 5, end: 6, title: "Lập trình hướng đối tượng Java" },
  ];

  const getCellProps = (day: number, period: number) => {
    const activity = activities.find(
      (act) => act.day === day && act.start <= period && act.end >= period
    );
    if (activity) {
      const { start, end } = activity;
      const rowSpan = end - start + 1;
      if (period === start) {
        return { rowSpan, style: { verticalAlign: "top" } };
      } else {
        return { style: { display: "none" } };
      }
      ``;
    }
    return {};
  };

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <Card
      title={
        <div className="flex justify-between">
          <p>Lịch thi</p>
          {/* <BackButton /> */}
        </div>
      }
    >
      <Drawer
        title="Basic Drawer"
        placement="right"
        onClose={onClose}
        open={open}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Drawer>
      <Row gutter={16}>
        <Col span={8}>
          <label>Năm</label>
          <Select
            className="w-full mt-2"
            options={YEAR}
            placeholder="Please choose"
          />
        </Col>
        <Col span={8}>
          <label>Tháng</label>
          <Select
            className="w-full mt-2"
            options={YEAR}
            placeholder="Please choose"
          />
        </Col>
        <Col span={8}>
          <label>Khoa</label>
          <Select
            className="w-full mt-2"
            options={YEAR}
            placeholder="Please choose"
          />
        </Col>
      </Row>
      <table className="schedule-table mt-6">
        <thead>
          <tr>
            <th
              style={{ background: "white", color: "#2fa4e7" }}
              className="flex items-center"
            >
              {"."}
            </th>
            {days.map((day) => (
              <th key={day}>{day}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {periods.map((period) => (
            <tr key={period}>
              <td className="text-white font-medium" style={{ width: "5%" }}>
                Tiết {period}
              </td>
              {days.map((day, index) => {
                let act = activities.find(
                  (act) => act.day === index + 1 && act.start === period
                );
                if (!act) return <td {...getCellProps(index + 1, period)}></td>;
                return (
                  <td
                    key={index}
                    {...getCellProps(index + 1, period)}
                    className="schedule-active"
                    onClick={showDrawer}
                  >
                    {getCellProps(index + 1, period).rowSpan ? (
                      <div>
                        <p
                          className="font-bold text-left"
                          style={{ fontSize: "small" }}
                        >
                          {act.title} {act?.idMh ? `(${act.idMh})` : null}
                        </p>
                        <p className="text-left" style={{ fontSize: "small" }}>
                          GV: <span>{act?.manager}</span>
                        </p>
                      </div>
                    ) : null}
                  </td>
                );
              })}
            </tr>
          ))}
        </tbody>
      </table>
    </Card>
  );
};

export default Schedule;

const YEAR = [
  {
    label: "2023",
    value: "2023",
  },
  {
    label: "2024",
    value: "2024",
  },
  {
    label: "2025",
    value: "2025",
  },
];
