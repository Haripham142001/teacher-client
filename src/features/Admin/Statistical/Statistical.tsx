import { Card, Col, Progress, Row, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import apiRequest from "services/api";
import helper from "services/helper";
import { DataResponse } from "services/model";

type Props = {};

interface IClass {
  sid: string;
  idLop: string;
  students: string[];
}

interface DataStaticstical {
  [field: string]: any;
}

interface IStatistical {
  classes: IClass[];
  data: DataStaticstical;
}

const Statistical = (props: Props) => {
  const location = useLocation();
  let pageId: string = location.search.split("?sid=")[1];
  const [data, setData] = useState<DataStaticstical>({});
  const [classes, setClasses] = useState<IClass[]>([]);
  const [selectClass, setSelectClass] = useState<string>("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const result: DataResponse<IStatistical> =
      await apiRequest.getStatisticalByExamConfig({
        sid: pageId,
      });
    if (result.errorCode === 0) {
      setClasses(result.data.classes);
      setData(result.data.data);
    }
  };

  return (
    <>
      <Col span={8}>
        <label>Chọn lớp</label>
        <Select
          className="w-full mt-2"
          options={helper.convertArrayToSelect(classes, "idLop", "sid")}
          value={selectClass}
          placeholder="Please choose"
          onChange={(e: string) => setSelectClass(e)}
        />
      </Col>
      {selectClass && (
        <Row gutter={16} className="mt-6">
          <Col span={8}>
            <label htmlFor="">Tỷ lệ sinh viên nộp bài</label>
            <br />
            <Progress
              className="mt-2"
              type="circle"
              percent={
                (data[selectClass].countSubmit /
                  data[selectClass].totalStudents) *
                100
              }
            />
          </Col>
        </Row>
      )}
    </>
  );
};

export default Statistical;
