import { EyeOutlined, LoginOutlined, WarningOutlined } from "@ant-design/icons";
import { Card, Col, Row, Statistic } from "antd";
import Button from "antd-button-color";
import moment from "moment";
import React, { useEffect, useState } from "react";
import apiRequest from "services/api";
import helper from "services/helper";
import { DataResponse, ExamForStudent } from "services/model";
import { useNavigate } from "react-router-dom";

const { Countdown } = Statistic;

type Props = {};
type FieldListExam = "others" | "today" | "completed" | "pending";

type ListExam = {
  [field in FieldListExam]?: ExamForStudent[];
};

let user: any = helper.getUser();
const LIST: { label: string; field: FieldListExam }[] = [
  {
    label: "Đang thi",
    field: "pending",
  },
  {
    label: "Hôm nay",
    field: "today",
  },
  {
    label: "Môn thi khác",
    field: "others",
  },
  {
    label: "Đã hoàn thành",
    field: "completed",
  },
];
const ExamList = (props: Props) => {
  const navigate = useNavigate();
  const [listExam, setListExam] = useState<ListExam>({});
  let now = new Date();
  let nowDate = moment(now).format("YYYY-MM-DD");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    let result: DataResponse<ListExam> = await apiRequest.getListExam({
      sid: user?.sid || "",
    });
    if (result.errorCode === 0) {
      setListExam(result.data);
    }
  };

  return (
    <>
      {LIST.map((ls, indexL) => {
        if (listExam?.[ls.field]?.length === 0) return null;
        return (
          <div key={indexL} className="mb-3">
            <p className="font-bold text-red-500 text-2xl">{ls.label}</p>
            <Row gutter={[16, 16]} className="mt-2">
              {listExam?.[ls.field]?.map((exam: ExamForStudent, index) => {
                return (
                  <Col span={12} md={8} xl={6} key={index}>
                    <Card title={exam.name} className="h-64">
                      <div className="h-28">
                        <p>
                          Môn:{" "}
                          <span className="font-bold ml-2 text-green-500">
                            {exam.subject?.name + "-" + exam.subject?.idMH}
                          </span>
                        </p>
                        <p>
                          Ngày:{" "}
                          {nowDate === exam.date ? (
                            <span className="font-bold ml-2 text-red-500">
                              {exam.date}
                            </span>
                          ) : (
                            <span className="font-bold ml-2 text-green-500">
                              {exam.date}
                            </span>
                          )}
                        </p>
                        <p>
                          Thời gian:{" "}
                          <span className="font-bold ml-2 text-green-500">
                            Tiết {exam.lession}
                          </span>
                        </p>
                        {ls.field === "pending" && (
                          <div className="flex">
                            <span>Thời gian còn lại:</span>
                            <Countdown
                              value={exam?.deadline}
                              onFinish={() => {}}
                              valueStyle={{
                                color: "red",
                                fontWeight: "bold",
                                textAlign: "center",
                                fontSize: "14px",
                              }}
                              className="ml-2"
                            />
                          </div>
                        )}
                      </div>

                      {ls.field === "completed" && exam?.isViewResult && (
                        <Button
                          className="w-full flex justify-center h-10"
                          type="warning"
                          onClick={() => navigate(`/exam/view?sid=${exam.sid}`)}
                        >
                          <EyeOutlined />
                          Xem kết quả
                        </Button>
                      )}
                      {ls.field === "others" && (
                        <Button
                          className="w-full flex justify-center h-10"
                          type="warning"
                          disabled
                        >
                          <WarningOutlined />
                          Chưa đến lịch hoặc hết hạn
                        </Button>
                      )}
                      {ls.field === "today" && (
                        <Button
                          className="w-full flex justify-center h-10"
                          type="info"
                          onClick={() =>
                            navigate(`/exam/start/${exam.sid}`, {
                              state: exam,
                            })
                          }
                        >
                          <LoginOutlined />
                          Bắt đầu thi
                        </Button>
                      )}
                      {ls.field === "pending" && (
                        <Button
                          className="w-full flex justify-center h-10"
                          type="success"
                          onClick={() =>
                            navigate(`/exam/do`, {
                              state: exam.exam_id,
                            })
                          }
                        >
                          <LoginOutlined />
                          Thi tiếp
                        </Button>
                      )}
                    </Card>
                  </Col>
                );
              })}
            </Row>
          </div>
        );
      })}
    </>
  );
};

export default ExamList;
