import { FormOutlined } from "@ant-design/icons";
import { Card, Col, Progress, Row } from "antd";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import apiRequest from "services/api";
import { DataResponse } from "services/model";
import { setArrAssignment, setCurrentAssignment } from "store/slices/dataSlice";
import { useAppDispatch } from "store/store";

type Props = {};

interface IAssignment {
  sid: string;
  name: string;
  countAllDoc: number;
  countSubmited: number;
}

const Assignment = (props: Props) => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [data, setData] = useState<IAssignment[]>([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const result: DataResponse<IAssignment[]> =
      await apiRequest.getAllClassesAssignmented();
    if (result?.errorCode === 0) {
      setData(result.data);
    }
  };

  const handleCheckPoint = async (sid: string) => {
    let arrSid: DataResponse<string[]> = await apiRequest.getExamsAssignment({
      sid,
    });
    if (arrSid.errorCode === 0) {
      dispatch(setArrAssignment(arrSid.data));
      navigate(`/check-essay?sid=${arrSid.data[0]}`, {
        state: {
          arr: arrSid.data,
        },
      });
    }
  };

  return (
    <Row gutter={16}>
      {data?.map((dt, index) => {
        return (
          <Col span={8} key={index}>
            <Card
              title={dt.name}
              actions={
                dt.countSubmited !== dt.countAllDoc
                  ? [
                      <div
                        className="flex items-center justify-center text-red-500 cursor-pointer"
                        onClick={() => handleCheckPoint(dt.sid)}
                      >
                        <FormOutlined className="mr-2" />
                        <span>Tiếp tục chấm</span>
                      </div>,
                    ]
                  : []
              }
            >
              <p>
                Số bài thi đã chấm {dt.countSubmited}/{dt.countAllDoc}
              </p>
              <div className="flex justify-center">
                <Progress
                  className="mt-2"
                  type="circle"
                  size="default"
                  percent={(dt.countSubmited / dt.countAllDoc) * 100}
                />
              </div>
            </Card>
          </Col>
        );
      })}
    </Row>
  );
};

export default Assignment;
