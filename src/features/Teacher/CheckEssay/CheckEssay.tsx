import {
  ArrowRightOutlined,
  CheckOutlined,
  DownloadOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Card, Col, Input, InputNumber, Row } from "antd";
import Button from "antd-button-color";
import FileUploader from "components/UI/FileUploader";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import apiRequest from "services/api";
import helper from "services/helper";
import { DataResponse, ExamQuestions } from "services/model";
import { selectArrAssignment } from "store/slices/dataSlice";

type Props = {};

interface IAssignment {
  sid: string;
  questions: ExamQuestions[];
  timeSubmit: string;
  setting: any;
  pointed: string;
}

const CheckEssay = (props: Props) => {
  const location = useLocation();
  const navigate = useNavigate();
  const selectArrAssignmentSelector = useSelector(selectArrAssignment);
  let pageId: string = location.search.split("?sid=")[1];

  const [data, setData] = useState<Partial<IAssignment>>({});
  const [currentIndex, setCurrentIndex] = useState<number>(0);

  useEffect(() => {
    if (location.state && selectArrAssignmentSelector.length > 0) {
      fetchData();
    } else {
      navigate(-1);
    }
  }, [pageId]);

  const fetchData = async () => {
    const result: DataResponse<IAssignment> =
      await apiRequest.getExamFromTeacher({
        sid: pageId,
      });
    if (result?.errorCode === 0) {
      setData(result.data);
    }
  };

  const handleOpenAttachment = async () => {
    let url =
      data.questions?.[currentIndex]?.value?.attachment?.toString() || "";
    const link = document.createElement("a");
    link.href = url;
    link.download = `Cau_${currentIndex + 1}_${helper.generateDigitCode(6)}`;
    link.click();
  };

  const handleChangeFieldQuestion = (field: string, value: any) => {
    const _data = Object.assign({}, data);
    if (_data?.questions?.[currentIndex]) {
      _data.questions[currentIndex][field] = value;
    }
    setData(_data);
  };

  const handleCheckPoint = async () => {
    for (let qs of data?.questions || []) {
      if (!qs.point) {
        if (Number(qs.point) === 0) continue;
        return helper.toast("error", "Một số câu chưa có điểm");
      }
    }
    const result = await apiRequest.checkPointByTeacher({
      sid: data.sid,
      questions: data.questions,
    });
    if (result.errorCode === 0) {
      fetchData();
      return helper.toast("success", "Đã chấm");
    }
  };

  const handleExamNext = () => {
    let _arr = location.state?.arr || [];
    let findAssignment = _arr?.findIndex((asm: string) => asm === pageId);
    if (findAssignment + 1 === _arr?.length) {
      return navigate("/assignment");
    } else {
      _arr.shift();
      return navigate(`/check-essay?sid=${_arr[0]}`, {
        state: {
          arr: _arr,
        },
      });
    }
  };

  let warehouse_id = data.questions?.[currentIndex]?.warehouse_id;
  let questionSetting = data.setting?.questions?.[warehouse_id || ""];

  return (
    <Card
      title={
        <div className="flex justify-between">
          <span>Chấm điểm tự luận</span>
          <div className="flex">
            <Button
              type="success"
              className="mr-1"
              onClick={handleCheckPoint}
              disabled={data?.pointed === "1"}
            >
              <EditOutlined />
              Chấm điểm
            </Button>
            <Button type="primary" onClick={handleExamNext}>
              Bài tiếp theo
              <ArrowRightOutlined />
            </Button>
          </div>
        </div>
      }
      style={{ height: "calc(100vh - 115px)" }}
    >
      <Row gutter={16} className="overflow-y-scroll">
        <Col span={6} className="border-r-2">
          <div className="flex flex-wrap">
            {data?.questions?.map((qs, index) => (
              <div
                key={index}
                onClick={() => setCurrentIndex(index)}
                className={`${
                  index === currentIndex && "bg-red-500 text-white border-none"
                } h-10 w-10 border border-gray-400 flex justify-center items-center mr-2 rounded-full cursor-pointer hover:bg-red-500 hover:border-none hover:text-white`}
              >
                {qs?.point?.point || qs?.point?.point === 0 ? (
                  <CheckOutlined />
                ) : (
                  index + 1
                )}
              </div>
            ))}
          </div>
        </Col>
        <Col span={18} className="overflow-y-scroll">
          <div style={{ height: "calc(100vh - 215px)" }}>
            <div className="mb-3 flex items-center justify-end">
              <InputNumber
                placeholder="Điểm"
                className="w-1/4"
                max={
                  (questionSetting?.totalPoint || 1) /
                  (questionSetting?.number || 1)
                }
                min={-1}
                value={data?.questions?.[currentIndex]?.point?.point}
                onChange={(e) =>
                  handleChangeFieldQuestion("point", {
                    point: e,
                    total: questionSetting?.totalPoint,
                  })
                }
              />
              <span className="text-3xl ml-3">
                /
                <b className="text-red-500 text-3xl ml-2">
                  {(questionSetting?.totalPoint || 1) /
                    (questionSetting?.number || 1)}
                </b>
              </span>
            </div>
            <div className="border rounded-lg p-4 mb-2">
              <b>Câu hỏi {currentIndex + 1}:</b>
              <div
                className="overflow-y-scroll"
                style={{
                  maxHeight: "200px",
                }}
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: (
                      data.questions?.[currentIndex]?.content || "<div></div>"
                    )?.replace(/(<? *script)/gi, "illegalscript"),
                  }}
                  className="h-full"
                />
              </div>
              <p>
                <b>Trả lời: </b>
              </p>
              <p className="text-red-500 font-medium">
                {data.questions?.[currentIndex]?.value.result}
              </p>
              <Button
                type="success"
                className="mt-3"
                onClick={handleOpenAttachment}
              >
                <DownloadOutlined /> Tải xuống câu trả lời
              </Button>
            </div>
            <div className="border rounded-lg p-4 mb-2">
              <b>Nhận xét của giáo viên:</b>
              <Input.TextArea
                className="mt-2"
                rows={5}
                value={data?.questions?.[currentIndex]?.teacherComment}
                onChange={(e) =>
                  handleChangeFieldQuestion("teacherComment", e.target.value)
                }
              />
            </div>
          </div>
        </Col>
      </Row>
    </Card>
  );
};

export default CheckEssay;
