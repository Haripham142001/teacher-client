import React, { ReactNode, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import apiRequest from "services/api";
import local from "services/local";
import { DataResponse, UserInfo } from "services/model";
import { setConfigs } from "store/slices/configsSlice";
import { useAppDispatch } from "store/store";

type Props = {
  children: ReactNode;
};

const FullView = ({ children }: Props) => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  useEffect(() => {
    let user: UserInfo = local.get("user", {});
    let token: string = local.get("token", "");
    if (!user || !token) {
      navigate("/login", { replace: true });
    }
    fetchData();
  }, []);

  const fetchData = async () => {
    const meta: DataResponse<{
      menus: any[];
      configs: {
        [key: string]: any;
      };
    }> = await apiRequest.getMeta();
    if (meta.errorCode === 0) {
      dispatch(setConfigs(meta?.data?.configs));
    }
  };

  return <div>{children}</div>;
};

export default FullView;
