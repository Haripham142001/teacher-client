import React, { ReactNode, useEffect, useState } from "react";
import Header from "./components/Header";
import HeaderStudent from "./components/Header/HeaderStudent";
import NavBar from "./components/NavBar";
import { useSelector } from "react-redux";
import { selectIsShowNav } from "store/slices/navSlice";
import { UserInfo } from "services/model";
import local from "services/local";
import { useNavigate } from "react-router-dom";

type Props = {
  children: ReactNode;
};

const MainLayout = ({ children }: Props) => {
  const navRef = React.useRef<HTMLDivElement>(null);
  const mainRef = React.useRef<HTMLDivElement>(null);
  const navigate = useNavigate();
  const isShow = useSelector(selectIsShowNav);
  const [role, setRole] = useState<string>("student");

  useEffect(() => {
    let user: UserInfo = local.get("user", {});
    try {
      let us = JSON.parse(user.toString());
      if (us?.role) {
        setRole(us.role);
      } else {
        setRole("student");
      }
    } catch (error) {}

    let token: string = local.get("token", "");
    if (!user || !token) {
      navigate("/login", { replace: true });
    }
  }, []);

  useEffect(() => {
    if (navRef.current && mainRef.current) {
      navRef.current.style.transition = "0.25s";
      mainRef.current.style.transition = "0.25s";
      if (isShow) {
        navRef.current.style.transform = "translateX(0px)";
        navRef.current.style.width = "280px";
        mainRef.current.style.transform = "translateX(0px)";
      } else {
        navRef.current.style.transform = "translateX(-280px)";
        navRef.current.style.width = "0px";
        mainRef.current.style.transform = "translateX(0px)";
      }
    }
  }, [isShow]);

  return (
    <div style={{ height: "calc(100vh - 64px)" }}>
      {role !== "student" ? <Header /> : <HeaderStudent />}
      <div className="flex mt-16">
        <div ref={navRef}>
          <NavBar />
        </div>
        <div
          ref={mainRef}
          className="w-full p-5"
          style={{ height: "calc(100vh - 64px)", overflowY: "scroll" }}
        >
          {children}
        </div>
      </div>
    </div>
  );
};

export default MainLayout;
