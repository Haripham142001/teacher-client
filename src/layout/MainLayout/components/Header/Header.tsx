import React from "react";
import { BiMenu } from "react-icons/bi";
import { useAppDispatch } from "store/store";

import "./header.scss";
import { setIsShowNav } from "store/slices/navSlice";
import { Link } from "react-router-dom";
import helper from "services/helper";

type Props = {};

const user: any = helper.getUser();

const Header = (props: Props) => {
  const dispatch = useAppDispatch();

  return (
    <header
      className="fixed top-0 right-0 left-0 z-10"
      style={{ boxShadow: "rgba(0, 0, 0, 0.15) 0px 3px 3px 0px" }}
    >
      <div className="left_header flex justify-between">
        <Link to="/">
          <div className="logo pl-8 text-2xl uppercase font-bold">
            {user?.role}
          </div>
        </Link>
        <div
          className="nav"
          onClick={() => {
            dispatch(setIsShowNav(false));
          }}
        >
          <BiMenu />
        </div>
      </div>
    </header>
  );
};

export default Header;
