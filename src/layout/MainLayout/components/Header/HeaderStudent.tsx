import React, { useState } from "react";
import { BiMenu } from "react-icons/bi";
import { useAppDispatch } from "store/store";

import "./header.scss";
import { setIsShowNav } from "store/slices/navSlice";
import { Link } from "react-router-dom";
import { Avatar, Badge, Space } from "antd";
import { LockOutlined, LogoutOutlined } from "@ant-design/icons";

type Props = {};

const Header = (props: Props) => {
  const dispatch = useAppDispatch();
  const [isShow, setIsShow] = useState<boolean>(false);

  const handleHideMenu = () => {
    setIsShow(!isShow);
  };

  return (
    <header
      className="fixed top-0 right-0 left-0 z-10 flex justify-between px-8"
      style={{ boxShadow: "rgba(0, 0, 0, 0.15) 0px 3px 3px 0px" }}
    >
      <div className="left_header flex justify-between">
        <Link to="/">
          <div className="logo text-2xl uppercase font-bold">Sinh viên</div>
        </Link>
        <div
          className="nav"
          onClick={() => {
            dispatch(setIsShowNav(false));
          }}
        >
          <BiMenu />
        </div>
      </div>
      <div className="h-16 flex items-center">
        <div className="relative">
          <Avatar
            src="https://i.pinimg.com/736x/6b/3d/29/6b3d29011e393343296ff339d5398ba1.jpg"
            size="large"
            onClick={handleHideMenu}
            className="relative cursor-pointer"
          />
          {isShow && (
            <div className="absolute bg-white border top-12 right-0 w-40 rounded h-max overflow-hidden">
              <div className="w-full h-10 flex items-center px-3 text-gray-500 cursor-pointer hover:bg-blue-200">
                <LockOutlined className="mr-2" />
                <span>Đổi mật khẩu</span>
              </div>
              <div className="w-full h-10 flex items-center px-3 text-gray-500 cursor-pointer hover:bg-blue-200">
                <LogoutOutlined className="mr-2" />
                <span>Đăng xuất</span>
              </div>
            </div>
          )}
        </div>
      </div>
    </header>
  );
};

export default Header;
