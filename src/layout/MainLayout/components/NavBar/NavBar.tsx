import React, { useEffect, useState } from "react";
import admin, { MenuItem } from "constant/admin";
import NavBarItem from "./components/NavBarItem";
import NavBarParent from "./components/NavBarParent";
import { useLocation } from "react-router-dom";

import "./navBar.scss";
import apiRequest from "services/api";
import { DataResponse } from "services/model";
import RenderIcon from "components/RenderIcon";
import { setConfigs } from "store/slices/configsSlice";
import { useAppDispatch } from "store/store";

type Props = {};

const NavBar = (props: Props) => {
  const location = useLocation(); // Sử dụng hook useLocation để lấy thông tin về URL hiện tại của trang
  const currentUrl = location.pathname + location.search; // Lấy đường dẫn (pathname) từ đối tượng location
  const dispatch = useAppDispatch();
  const [menu, setMenu] = useState<MenuItem[]>([]);

  useEffect(() => {
    getMenu();
  }, []);

  const getMenu = async () => {
    const meta: DataResponse<{
      menus: any[];
      configs: {
        [key: string]: any;
      };
    }> = await apiRequest.getMeta();
    if (meta?.errorCode === 0) {
      let _menu = meta?.data?.menus?.map((mn) => {
        return {
          content: mn?.name,
          isParent: mn?.isParent,
          icon: mn?.icon || "AppstoreOutlined",
          link: mn?.url,
          child: mn?.child?.map((ch: any) => {
            return {
              content: ch?.name,
              isParent: ch?.isParent,
              icon: "SendOutlined",
              link: ch?.url,
            };
          }),
        };
      });
      setMenu(_menu);
      dispatch(setConfigs(meta?.data?.configs));
    }
  };

  return (
    <div className="navbar bg-white" style={{ height: "calc(100vh - 64px)" }}>
      <ul className="pl-3">
        {menu?.map((mn, index) => {
          if (!mn.isParent) {
            return (
              <li key={index} className="w-100">
                <NavBarItem {...mn} isActive={currentUrl} />
              </li>
            );
          } else {
            return (
              <li key={index} className="w-100">
                <NavBarParent {...mn} isActive={currentUrl} />
              </li>
            );
          }
        })}
      </ul>
    </div>
  );
};

export default NavBar;
