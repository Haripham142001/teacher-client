import React from "react";

import { MenuItem } from "constant/admin";

import "./menuItem.scss";
import { Link } from "react-router-dom";
import RenderIcon from "components/RenderIcon";

const NavBarItem = (props: MenuItem) => {
  return (
    <Link to={props.link}>
      <div
        className={`menu-item ${
          props?.isActive === props?.link ? "active" : ""
        } flex rounded py-4 px-3`}
      >
        <div className="flex justify-center h-100 w-7">
          {/* {RenderIcon(props?.icon)} */}
          <RenderIcon type={props?.icon || "DownCircleOutlined"} />
        </div>
        <div className="ml-2">{props?.content}</div>
      </div>
    </Link>
  );
};

export default NavBarItem;
