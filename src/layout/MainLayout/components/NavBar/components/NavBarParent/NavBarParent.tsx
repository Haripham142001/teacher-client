import { MenuItem } from "layout/MainLayout/menu";
import { FaAngleDown } from "react-icons/fa";
import React, { useState, useRef } from "react";
import NavBarItem from "../NavBarItem";
import RenderIcon from "components/RenderIcon";

const NavBarParent = (props: MenuItem) => {
  const divRef = useRef<HTMLDivElement>(null);
  const [isShow, setIsShow] = useState<boolean>(false);
  return (
    <>
      <div
        className="menu-item relative flex rounded py-4 px-3 cursor-pointer"
        onClick={() => {
          if (divRef.current) {
            divRef.current.style.transition = "0.25s";
            if (!isShow) {
              divRef.current.style.transform = "rotate(180deg)";
            } else {
              divRef.current.style.transform = "rotate(0deg)";
            }
          }
          setIsShow(!isShow);
        }}
      >
        <div className="flex justify-center h-100 w-7">
          <RenderIcon type={props.icon} />
        </div>
        <div className="ml-2">{props.content}</div>
        <div className="absolute right-4" ref={divRef}>
          <FaAngleDown className="scale-50" />
        </div>
      </div>
      {isShow && (
        <ul className="ml-7 child">
          {props?.child?.map((ch: any, index: number) => {
            return (
              <li key={index}>
                <NavBarItem {...ch} isActive={props?.isActive} />
              </li>
            );
          })}
        </ul>
      )}
    </>
  );
};

export default NavBarParent;
