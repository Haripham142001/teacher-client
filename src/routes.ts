import DynamicTable from "components/DynamicTable";
import React, { ReactNode } from "react";

const Dashboard = React.lazy(() => import("views/Dashboard"));
const PageEditor = React.lazy(() => import("features/Admin/PageEditor"));
const QuestionAction = React.lazy(
  () => import("features/Admin/QuestionBank/QuestionAction")
);
const ListStudent = React.lazy(() => import("features/Admin/ListStudent"));
const ImportListStudent = React.lazy(
  () => import("features/Admin/ListStudent/components/ImportListStudent")
);
const ImportData = React.lazy(() => import("features/Admin/ImportData"));
const Schedule = React.lazy(() => import("features/Admin/Schedule"));
const ExamConfig = React.lazy(() => import("features/Admin/ExamConfig"));
const ExamFrame = React.lazy(() => import("features/Admin/ExamFrame"));
const Statistical = React.lazy(() => import("features/Admin/Statistical"));
const Assignment = React.lazy(() => import("features/Teacher/Assignment"));
const CheckEssay = React.lazy(() => import("features/Teacher/CheckEssay"));

const ExamList = React.lazy(() => import("features/Student/ExamList"));
const ExamStart = React.lazy(() => import("views/Exam/ExamStart"));
const DoExam = React.lazy(() => import("views/Exam/DoExam"));
const ViewExam = React.lazy(() => import("views/Exam/ViewExam"));

export const routes = [
  {
    path: "/",
    component: Dashboard,
  },
  {
    path: "/pageEditor",
    component: PageEditor,
  },
  {
    path: "/page",
    component: DynamicTable,
  },
  {
    path: "/question",
    component: QuestionAction,
  },
  {
    path: "/listStudent",
    component: ListStudent,
  },
  {
    path: "/importListStudent",
    component: ImportListStudent,
  },
  {
    path: "/import",
    component: ImportData,
  },
  {
    path: "/schedule",
    component: Schedule,
  },
  {
    path: "/examconfig",
    component: ExamConfig,
  },
  {
    path: "/examframe",
    component: ExamFrame,
  },
  {
    path: "/examlist",
    component: ExamList,
  },
  {
    path: "/statistical",
    component: Statistical,
  },
  {
    path: "/assignment",
    component: Assignment,
  },
  {
    path: "/check-essay",
    component: CheckEssay,
  },
];

export const fullViewRoutes = [
  {
    path: "/start/:examId",
    component: ExamStart,
  },
  {
    path: "/do",
    component: DoExam,
  },
  {
    path: "/view",
    component: ViewExam
  }
];
