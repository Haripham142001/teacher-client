import { DataResponse } from "./model";
import request from "./request";

type ValueApi = (data?: any, headers?: any) => Promise<DataResponse<any>>;

interface API {
  [field: string]: string;
}

interface ApiRequest {
  [field: string]: ValueApi;
}

const api: API = {
  login: "/account/login",
  createPage: "/page/createPage",
  getPageInfoBySid: "/page/getPageInfoBySid",
  updatePage: "/page/updatePage",
  getMeta: "/menu/getMeta",
  createQuestion: "/question/createQuestion",
  getQuestionBySid: "/question/getQuestionBySid",
  updateQuestion: "/api/update/questions",
  getAllWarehouse: "/warehouse/getAll",
  getAllWarehouseDefault: "/api/findAll/warehouses",
  getAllSubject: "/api/find/subjects",
  getListStudentByClassId: "/class/getListStudentByClassId",
  uploadFile: "/others/uploadFile",
  validateDataUpload: "/others/validateDataUpload",
  importStudent: "/others/importStudent",
  getAllSpecialization: "/spe/getAll",
  getAllClass: "/class/getAllClass",
  updateExamConfig: "/examconfig/updateExamConfig",
  updateExamFrame: "/api/update/examframes",
  getExamConfigBySid: "/examconfig/getExamConfigBySid",
  getExamFrameBySid: "/examframe/getExamFrameBySid",
  getAllExamFrameDetails: "/examframe/getAllExamFrameDetails",
  getStatisticalByExamConfig: "/examconfig/getStatisticalByExamConfig",
  getAllTeacher: "/account/getAllTeacher",
  getDashBoardAdmin: "/others/getDashBoardAdmin",
  getNumberQuestion: "/question/getNumberQuestion",

  //student
  getListExam: "/examconfig/getListExam",
  getExamConfigBySidForStudent: "/examconfig/getExamConfigBySidForStudent",
  createExam: "/exam/createExam",
  getExam: "/exam/getExam",
  submitExam: "/exam/submitExam",
  getDashBoardTeacher: "/others/getDashBoardTeacher",

  // teacher
  getAllClassesAssignmented: "/class/getAllClassesAssignmented",
  getExamsAssignment: "/class/getExamsAssignment",
  getExamFromTeacher: "/exam/getExamFromTeacher",
  checkPointByTeacher: "/exam/checkPointByTeacher",
  getAllStudentByClassIdFromTeacher: "/class/getAllStudentByClassIdFromTeacher",
  getDashBoard: "/others/getDashBoard",
  getExamByStudent: "/exam/getExamByStudent"
};

const apiRequest: ApiRequest = {};

for (let key of Object.keys(api)) {
  apiRequest[key] = async (data?: any, headers?: any) => {
    let result: DataResponse<any> = await request.post(api[key], data, headers);
    return result;
  };
}

export default apiRequest;
