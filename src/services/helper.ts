import { toast } from "react-toastify";
import moment from "moment";
import { StudentUser, UserInfo } from "./model";
import local from "./local";

type TypeToast = "success" | "error" | "warning" | "info";
type USER = UserInfo | StudentUser;

interface Helper {
  toast: (type: TypeToast, content: string) => any;
  convertArrayToSelect: (
    arr: any[],
    fieldLabel: string,
    fieldValue: string
  ) => any[];
  convertItemToValueSelect: (
    obj: any,
    fieldLabel: string,
    fieldValue: string
  ) => any;
  replaceURL: (str: string, obj: any) => string;
  generateDigitCode: (lenth: number) => string;
  getUser: () => USER;
}

const helper: Helper = {
  toast: () => {},
  convertArrayToSelect: (arr, fieldLabel, fieldValue) => {
    return arr.map((a) => {
      return {
        label: a[fieldLabel],
        value: a[fieldValue],
        disabled: a?.disabled || false,
      };
    });
  },
  convertItemToValueSelect: (obj, fieldLabel, fieldValue) => {
    return {
      label: obj?.[fieldLabel] || "",
      value: obj?.[fieldValue] || "",
    };
  },
  replaceURL(string, obj) {
    let _string = string.split("#");
    let keys = Object.keys(obj);
    _string.forEach((str, index) => {
      if (keys.includes(str)) {
        _string[index] = obj[str];
      }
    });
    return _string.join("");
  },
  generateDigitCode(length) {
    length = Math.max(length, 6);
    let dateFactor = moment().diff(
      moment(new Date(2018, 0, 0, 0, 0, 0, 0)),
      "milliseconds"
    );
    let randomFator = ("000" + Math.round(Math.random() * 1000)).slice(-3);
    return ("000" + dateFactor).slice(-((length || 15) - 3)) + randomFator;
  },
  getUser() {
    try {
      let user = local.get("user", {});
      return JSON.parse(user);
    } catch (error) {
      return {};
    }
  },
};

helper.toast = (type, content) => toast?.[type](content);

export default helper;
