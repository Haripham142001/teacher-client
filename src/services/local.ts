interface Local {
  get: (key: string, defaultValue: any) => any;
  set: (key: string, value: any) => any;
  remove: (key: string) => any;
  clear: () => any;
}

const local: Local = {
  get: (key, defaultValue) => {
    try {
      return JSON.parse(JSON.stringify(sessionStorage.getItem(key)));
    } catch (error) {
      return defaultValue;
    }
  },
  set: (key, value) => {
    if (typeof value === "string") {
      sessionStorage.setItem(key, value);
    } else {
      sessionStorage.setItem(key, JSON.stringify(value));
    }
  },
  remove: (key) => {
    sessionStorage.removeItem(key);
  },
  clear: () => {
    sessionStorage.clear();
  },
};

export default local;
