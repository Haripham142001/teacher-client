export type PageInfoField = "buttons" | "form" | "grid" | "apis";
export type ColorSupport =
  | "succes"
  | "danger"
  | "primary"
  | "warning"
  | "default"
  | "info";
export type Method = "post" | "get";
type AdvanceExamConfig = {
  [field in FieldAdvanceExamConfig]?: any;
};

export type Lession =
  | "1"
  | "2"
  | "3"
  | "4"
  | "5"
  | "6"
  | "7"
  | "8"
  | "9"
  | "10"
  | "11"
  | "12";

export interface PageInfo {
  sid?: string;
  name?: string;
  client?: string;
  firstApi?: string;
  buttons: ButtonItem[];
  grid: GridItem[];
  form: FormItem[];
  apis: ApiItem[];
}

export interface FormItem {
  name: string;
  type: "string" | "number" | "boolean" | "json" | "array";
  field: string;
  required: boolean;
  interface: string;
  default: string;
  apiGetData: string;
  options: [];
  fieldDisplay: string;
}

export interface ButtonItem {
  name: string;
  field: string;
  color?: string;
  icon?: string;
  active: string;
  confirm?: string;
  view?: boolean;
  navigation?: string;
  buttonType: string;
  ref?: string;
  refVal?: string;
  api?: string;
  modal?: string;
  size?: number;
  dataEmbedded?: string;
}

export interface GridItem {
  name: string;
  field: string;
  type: string;
  filter?: boolean;
  fixed?: boolean;
  width?: string;
  success?: string;
  warning?: string;
  danger?: string;
  info?: string;
  primary?: string;
  badges?: boolean;
}

export interface ApiItem {
  name: string;
  url: string;
  method: Method;
  field: string;
  typeApi?: string;
}

export interface DataResponse<T> {
  errorCode: number;
  data: T;
  errorMsg?: string;
  count?: number;
}

export interface UserInfo {
  role: "admin" | "teacher" | "student";
  sid: string;
  username: string;
  _id: string;
}

export interface StudentUser {
  mssv: string;
  name: string;
  specialization_id: string;
}

export interface UserTokenInfo {
  token: string;
  user: UserInfo;
}

export interface Question {
  sid: string;
  content: string;
  type: string;
  config: ConfigQuestion;
  number: number;
  instruct?: string;
  subject_id?: string;
  specialization_id?: string;
  status: string;
  warehouse_id: string[];
  attachment?: string;
  sound?: string;
  point?: {
    point: number;
    total: number;
  };
  level?: string;
}

export interface ConfigQuestion {
  answer_correct?: string | string[];
  answers?: Answer[];
  items?: [];
  advance: {
    [field: string]: any;
  };
}

export interface Answer {
  key: string;
  label?: string;
  isCorrect?: boolean;
  options?: {
    label: string;
    key: string;
  }[];
  isGroup?: boolean;
  correct?: string;
  content?: string;
}

export interface WareHouse {
  readonly sid: string;
  name: string;
  subject_id: string;
  level: string;
  subject_name?: string;
  number: number;
  type: string;
}

export interface Subject {
  readonly sid: string;
  name: string;
  idMH: string;
}

export interface ClassInfo {
  idLop: string;
  major_id: string;
  manager: string;
  semester: string;
  readonly sid: string;
  status: string;
  students: string[];
  subject_id: string;
  year: string;
  major_name?: string;
  subject_name?: string;
}

export interface Specialization {
  sid: string;
  name: string;
  major_id: string;
}
export interface IExamConfig {
  sid: string;
  name: string;
  date: string;
  lession: string;
  time: string;
  classes: string[];
  semester: string;
  year: string;
  status: string;
  major_id: string;
  subject_id: string;
  subject?: Subject;
  advance: AdvanceExamConfig;
  classesInfo?: {
    sid: string;
    name: string;
  }[];
  assignment?: {
    [field: string]: {
      class_id: string;
      manager: string;
    };
  };
}

export type FieldAdvanceExamConfig =
  | "allowViewResult"
  | "autoGeneratePass"
  | "timeAllowLate"
  | "timeAutoCheckPoint"
  | "allowViewResultWhenCompleted"
  | "allowViewOthersPoint"
  | "allowAutoCheckPoint";

export interface IExamFrame {
  sid: string;
  name: string;
  status: string;
  year: string;
  setting: SettingExamFrame;
  subject_id: string;
  subject?: Subject;
  warehouse_id: string;
}

export interface SettingExamFrame {
  number: number;
  questions?: QuestionSettingExamFrame[];
  type: string;
  totalPoint: number;
}

export interface QuestionSettingExamFrame {
  type: string;
  number: number;
  totalPoint: number;
  count?: number;
  countEasy?: number;
  countMedium?: number;
  countDiff?: number;
}

export interface ExamForStudent {
  sid: string;
  date: string;
  lession: Lession;
  subject?: Subject;
  name: string;
  exam_id?: string;
  deadline?: string;
  advance: {
    [field: string]: any;
  };
  isViewResult?: boolean;
}

export interface Exam {
  examconfig_id: string;
  sid: string;
  mssv: string;
  questions: ExamQuestions[];
  name: string;
  subject: string;
  time: string;
  advance?: {
    [field: string]: any;
  };
  lession: string;
}

export interface ExamQuestions {
  sid: string;
  content: string;
  type: string;
  value?: any;
  answers?: {
    key: string;
    label: string;
    content?: string;
    isGroup?: boolean;
    options?: {
      label: string;
      key: string;
    }[];
  }[];
  flag?: boolean;
  advance: {
    [field: string]: any;
  };
  warehouse_id: string;
  teacherComment?: string;
  point?: {
    total?: number;
    point?: number;
  };
  instruct?: string;
  attachment?: string;
  sound?: string;
  missArr?: string[];
  isCorrect?: boolean;
}
