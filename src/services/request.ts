import { HOST } from "constant/global";
import helper from "./helper";
import local from "./local";

const request = {
  post: async function (
    endpoint: string,
    data: any,
    header?: any
  ): Promise<any> {
    const response = await fetch(`${HOST}${endpoint}`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: local.get("token", null),
        ...header,
      },
    });
    if (!response.ok) {
      throw new Error("Lỗi khi gọi API");
    }
    const responseData = await response.json();
    if (responseData.errorCode !== 0) {
      if (responseData.errorCode === 14) {
        setTimeout(() => {
          helper.toast("error", "Hết phiên đăng nhập. Vui lòng đăng nhập lại");
        }, 2000);
        local.clear();
        location.reload();
      }
      return helper.toast("error", responseData?.errorMsg || "Server Error");
    }
    return responseData;
  },
  get: async function (
    endpoint: string,
    data: any,
    header?: any
  ): Promise<any> {
    const response = await fetch(`${HOST}${endpoint}`, {
      method: "GET",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: local.get("token", null),
        ...header,
      },
    });
    if (!response.ok) {
      throw new Error("Lỗi khi gọi API");
    }
    const responseData = await response.json();
    if (responseData.errorCode !== 0) {
      return helper.toast("error", responseData?.errorMsg || "Server Error");
    }
    return responseData;
  },
};

export default request;
