import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

interface Configs {
  [key: string]: any;
}

const initialState: Configs = {};

export const configSlice = createSlice({
  name: "configs",
  initialState,
  reducers: {
    setConfigs: (state, action: PayloadAction<{ [key: string]: any }>) => {
      return action.payload;
    },
  },
});

export const { setConfigs } = configSlice.actions;

export default configSlice.reducer;

export const selectConfig = (state: RootState) => state.configs;
