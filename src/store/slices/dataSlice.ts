import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

interface Data {
  arrAssignment: string[];
  currentAssignment: number;
}

const initialState: Data = {
  arrAssignment: [],
  currentAssignment: 0,
};

export const dataSlice = createSlice({
  name: "data",
  initialState,
  reducers: {
    setArrAssignment: (state, action: PayloadAction<string[]>) => {
      state.arrAssignment = action.payload;
    },
    setCurrentAssignment: (state, action: PayloadAction<number>) => {
      state.currentAssignment = action.payload;
    },
  },
});

export const { setArrAssignment, setCurrentAssignment } = dataSlice.actions;

export default dataSlice.reducer;

export const selectArrAssignment = (state: RootState) =>
  state.data.arrAssignment;
export const selectCurrentAssignment = (state: RootState) =>
  state.data.currentAssignment;
