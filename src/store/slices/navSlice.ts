import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

interface SomeState {
  isShow: boolean;
}

const initialState: SomeState = {
  isShow: true,
};

export const navSlice = createSlice({
  name: "navbar",
  initialState,
  reducers: {
    setIsShowNav: (state, action: PayloadAction<boolean>) => {
      state.isShow = !state.isShow;
    },
  },
});

export const { setIsShowNav } = navSlice.actions;

export default navSlice.reducer;

export const selectIsShowNav = (state: RootState) => state.nav.isShow;
