import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

import { PageInfo, PageInfoField } from "services/model";

const initialState: PageInfo = {
  buttons: [],
  form: [],
  grid: [],
  apis: [],
};

export const pageEditorSlice = createSlice({
  name: "pageEditor",
  initialState,
  reducers: {
    setPageInfoStore: (state, action: PayloadAction<PageInfo>) => {
      let { buttons, form, grid, apis } = action.payload;
      state.buttons = buttons;
      state.form = form;
      state.grid = grid;
      state.apis = apis;
    },
    addItem: (
      state,
      action: PayloadAction<{
        schema: PageInfoField;
        data: any;
      }>
    ) => {
      state[action.payload.schema].push(action?.payload?.data);
    },
    deleteItem: (
      state,
      action: PayloadAction<{
        schema: PageInfoField;
        data: number;
      }>
    ) => {
      state[action.payload.schema].splice(action?.payload?.data, 1)
    },
    changeField: (
      state,
      action: PayloadAction<{
        schema: PageInfoField;
        value: any;
        field: string;
        index: number;
      }>
    ) => {
      let { schema, value, field, index } = action.payload;
      let newState = state?.[schema]?.[index];
      newState = {
        ...newState,
        [field]: value,
      };
      state[schema][index] = newState;
    },
  },
});

export const { setPageInfoStore, addItem, changeField, deleteItem } =
  pageEditorSlice.actions;

export default pageEditorSlice.reducer;

export const selectPageEditor = (state: RootState) => state.pageEditor;
export const selectButtons = (state: RootState) => state.pageEditor.buttons;
export const selectApis = (state: RootState) => state.pageEditor.apis;
