import { configureStore } from "@reduxjs/toolkit";
import navReducer from "./slices/navSlice";
import pageEditorReducer from "./slices/pageEditorSlice";
import { useDispatch } from "react-redux";
import configReducer from "./slices/configsSlice";
import dataReducer from "./slices/dataSlice";

// Cấu hình store Redux Toolkit
const store = configureStore({
  reducer: {
    nav: navReducer,
    pageEditor: pageEditorReducer,
    configs: configReducer,
    data: dataReducer,
  },
});

// Export store để sử dụng trong dự án
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export default store;
