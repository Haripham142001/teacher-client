import { ArrowUpOutlined } from "@ant-design/icons";
import { Card, Col, Row, Statistic } from "antd";
import CountUp from "react-countup";
import helper from "services/helper";
import { useEffect, useState } from "react";
import apiRequest from "services/api";
import Admin from "./components/Admin";

type Props = {};

const formatter: any = (value: number) => <CountUp end={value} />;

const user: any = helper.getUser();

const Dashboard = (props: Props) => {
  const [data, setData] = useState<any>({});

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    let api = "";
    switch (user?.role) {
      case "teacher":
        api = "getDashBoardTeacher";
        break;
      case "admin":
        api = "getDashBoardAdmin";
        break;
      default:
        api = "getDashBoard";
        break;
    }
    const result = await apiRequest[api]();
    if (result.errorCode === 0) {
      setData(result.data);
    }
  };

  return <div>{user?.role === "admin" && <Admin data={data} />}</div>;
};

export default Dashboard;
