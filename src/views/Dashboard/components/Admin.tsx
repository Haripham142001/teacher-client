import { ArrowUpOutlined } from "@ant-design/icons";
import { Card, Col, Row, Statistic } from "antd";
import React from "react";
import CountUp from "react-countup";

type Props = {
  data: any;
};

const formatter: any = (value: number) => <CountUp end={value} />;

const Admin = (props: Props) => {
  console.log(Object.keys(props?.data?.count || {}));

  return (
    <div>
      <Row gutter={16}>
        {Object.keys(props?.data?.count || {})?.map(
          (dt: any, index: number) => {
            return (
              <Col span={6} key={index}>
                <Card bordered={false}>
                  <Statistic
                    title={LABEL?.[dt]?.label || ""}
                    value={props?.data?.count?.[dt]}
                    precision={20}
                    valueStyle={{ color: LABEL?.[dt]?.color }}
                    // prefix={<ArrowUpOutlined />}
                    suffix=""
                    formatter={formatter}
                  />
                </Card>
              </Col>
            );
          }
        )}
      </Row>
    </div>
  );
};

export default Admin;

const LABEL = {
  accounts: {
    label: "Số lượng tài khoản",
    color: "red",
  },
  exams: {
    label: "Lượt thi",
    color: "blue",
  },
  questions: {
    label: "Số lượng câu hỏi",
    color: "green",
  },
  students: {
    label: "Số lượng sinh viên",
    color: "brown",
  },
};
