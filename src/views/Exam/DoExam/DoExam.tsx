import { Card, Collapse, Image, Popconfirm, Statistic } from "antd";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import apiRequest from "services/api";
import {
  DataResponse,
  Exam,
  ExamForStudent,
  StudentUser,
} from "services/model";
import SingleChoice from "./components/SingleChoice";
import Button from "antd-button-color";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  BulbOutlined,
  CheckOutlined,
  CheckSquareOutlined,
  FlagFilled,
  FlagOutlined,
  GroupOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import helper from "services/helper";
import DoEssay from "./components/DoEssay";
import MultiChoice from "./components/MultiChoice";
import GroupQuestion from "./components/GroupQuestion";
import moment from "moment";
import { useSelector } from "react-redux";
import { selectConfig } from "store/slices/configsSlice";
import dayjs from "dayjs";
import AudioPlayer from "react-h5-audio-player";

type Props = {};

const { Countdown } = Statistic;

const user: any = helper.getUser();

const { Panel } = Collapse;

const DoExam = (props: Props) => {
  const config = useSelector(selectConfig);
  const location = useLocation();
  const navigate = useNavigate();
  const state: ExamForStudent = location.state;
  const [data, setData] = useState<Partial<Exam>>({});
  const [current, setCurrent] = useState<number>(0);

  useEffect(() => {
    if (!state) {
      return navigate("/");
    } else {
      fetchData();
    }
  }, []);

  const fetchData = async () => {
    const result: DataResponse<Exam> = await apiRequest.getExam({
      sid: state,
    });
    if (result.errorCode === 0) {
      setData(result.data);
    }
  };

  const handleChangeAnswer = (value: any) => {
    let _questions = [...(data?.questions || [])];
    _questions[current].value = value;
    setData({
      ...data,
      questions: _questions,
    });
  };

  const handleSubmitExam = async () => {
    let result: DataResponse<any> = await apiRequest.submitExam({
      ...data,
    });
    if (result?.errorCode === 0) {
      helper.toast("success", "Đã nộp bài");
      navigate("/");
    }
  };

  const handleAddFlag = () => {
    let _questions = [...(data?.questions || [])];
    _questions[current].flag = !_questions[current]?.flag || false;
    setData({
      ...data,
      questions: _questions,
    });
  };

  let Question;
  switch (data.questions?.[current]?.type) {
    case "Tự luận":
      Question = (
        <DoEssay
          questions={data?.questions?.[current] || {}}
          onChange={(value: any, field: string) => {
            let _value = data?.questions?.[current]?.value || {};
            _value[field] = value;
            handleChangeAnswer(_value);
          }}
        />
      );
      break;
    case "Nhiều phương án":
      Question = (
        <MultiChoice
          questions={data?.questions?.[current] || {}}
          onChange={(value: any) => handleChangeAnswer(value)}
        />
      );
      break;
    case "Nhóm câu hỏi":
      Question = (
        <GroupQuestion
          questions={data?.questions?.[current] || {}}
          onChange={(value: any) => handleChangeAnswer(value)}
        />
      );
      break;
    default:
      Question = (
        <SingleChoice
          questions={data?.questions?.[current] || {}}
          onChange={(value: any) => handleChangeAnswer(value)}
        />
      );
      break;
  }

  let que = data?.questions?.[current];
  let dateExam =
    moment().format("YYYY-MM-DD") +
    " " +
    config?.["TIME_LESSION"]?.subConfigs?.[data?.lession || "12"]?.value;
  let deadline = new Date(
    new Date(dateExam).getTime() + (Number(data?.time) || 0) * 60 * 1000
  );
  let deadline2: dayjs.Dayjs = dayjs(deadline);

  return (
    <div className="grid grid-cols-12 h-screen">
      <div className="col-span-9 h-screen overflow-x-auto p-4 scroll_view">
        <Card
          title={
            <div className="text-xl h-max flex justify-between">
              <span>Câu thứ {current + 1}</span>
              <span onClick={handleAddFlag}>Gắn cờ</span>
            </div>
          }
        >
          <div
            className="sticky -top-4 z-10 bg-white overflow-hidden rounded-lg"
            style={{
              boxShadow:
                "rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px",
            }}
          >
            <Collapse defaultActiveKey={["1"]}>
              <Panel header="Câu hỏi" key="1" className="bg-blue-400">
                <div
                  className="overflow-y-scroll"
                  style={{
                    maxHeight: "200px",
                  }}
                >
                  <div
                    dangerouslySetInnerHTML={{
                      __html: (que?.content || "<div></div>")?.replace(
                        /(<? *script)/gi,
                        "illegalscript"
                      ),
                    }}
                    className="h-full"
                  />
                </div>
              </Panel>
            </Collapse>
          </div>
          {que?.sound && (
            <AudioPlayer
              // autoPlay
              src={que.sound}
              onPlay={(e) => console.log("onPlay")}
              className="my-3"
            />
          )}
          {que?.attachment &&
            ["png", "jpg"].includes(
              que.attachment.substring(que.attachment.lastIndexOf(".") + 1)
            ) && (
              <img
                src={que.attachment}
                alt="Image"
                width="100%"
                className="mb-4"
              />
            )}
          {que?.instruct && (
            <div className="h-28 border p-3 bg-yellow-200 overflow-y-scroll mt-3 rounded-m scroll_view">
              <p>
                <b className="flex items-center">
                  <BulbOutlined className="scale-150 mr-2" />
                  <span>Gợi ý:</span>
                </b>
              </p>
              <span>{que?.instruct}</span>
            </div>
          )}
          <hr className="mb-6" />
          {Question}
          <div className="h-3"></div>
          <span
            className="cursor-pointer hover:underline hover:text-red-500"
            onClick={() => handleChangeAnswer(null)}
          >
            Xóa đáp án
          </span>
          <div className="flex justify-center">
            <Button
              type="primary"
              onClick={() => {
                if (current > 0) {
                  setCurrent((cur) => (cur -= 1));
                }
              }}
              disabled={current === 0}
            >
              <ArrowLeftOutlined />
              Câu trước
            </Button>
            <Button
              type="success"
              onClick={() => {
                if (current < (data?.questions?.length || 40) - 1) {
                  setCurrent((cur) => (cur += 1));
                }
              }}
              className="ml-1"
              disabled={current === (data?.questions?.length || 1) - 1}
            >
              Câu tiếp
              <ArrowRightOutlined />
            </Button>
          </div>
        </Card>
      </div>
      <div
        className="col-span-3 h-screen p-4 relative"
        style={{
          boxShadow:
            "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
        }}
      >
        <div className="flex">
          <Image
            src="https://i.pinimg.com/736x/6b/3d/29/6b3d29011e393343296ff339d5398ba1.jpg"
            className="rounded-full"
            width={250}
          />
          <div className="w-full ml-5">
            <p>
              Môn thi:
              <span className="font-bold text-red-400">
                {" " + data.subject}
              </span>
            </p>
            <p>
              Mssv: <span className="font-bold text-red-400">{user?.mssv}</span>
            </p>
            <p>
              Họ và tên:{" "}
              <span className="font-bold text-red-400">{user?.name}</span>
            </p>
            <p className="text-red-500 mt-3">
              <FlagFilled />
              <span className="text-xs">Câu đã chọn nhưng chưa chắc chắn</span>
            </p>
            <p className="text-blue-500">
              <GroupOutlined />
              <span className="text-xs">Nhóm câu hỏi</span>
            </p>
          </div>
        </div>
        <div className="w-1/2">
          {/* <Countdown
            value={
              deadline2 ? deadline2?.toISOString() : new Date().toISOString()
            }
            onFinish={handleSubmitExam}
            valueStyle={{
              color: "blue",
              fontWeight: "bold",
              textAlign: "left",
              fontSize: "40px",
            }}
            title={<p className="mt-3">Thời gian làm bài</p>}
          /> */}
        </div>
        <ul className="flex flex-wrap pl-2 pt-4 overflow-y-scroll scroll_view">
          {data?.questions?.map((qs, index) => {
            let val = data.questions?.[index]?.value;
            let que = data.questions?.[index];
            let icon;
            if (que?.type === "Nhóm câu hỏi") {
              icon = <GroupOutlined />;
            } else if (val) {
              icon = <CheckOutlined className="text-white" />;
            } else {
              icon = index + 1;
            }

            return (
              <li
                key={qs.sid + "circle"}
                onClick={() => setCurrent(index)}
                className={`${
                  current === index
                    ? "outline-red-400 outline-4 outline"
                    : "border border-gray-400"
                } ${
                  val && "bg-blue-400"
                } relative h-10 w-10 mr-2 mb-2 rounded-full flex items-center justify-center cursor-pointer hover:bg-blue-400`}
              >
                {icon}
                {data.questions?.[index]?.flag && (
                  <FlagFilled className="absolute text-red-500 -top-2 -right-2" />
                )}
              </li>
            );
          })}
        </ul>
        <div className="absolute h-max bottom-0 w-full left-0 p-4">
          <Popconfirm
            placement="bottom"
            title="Bạn có muốn kết thúc làm bài không?"
            onConfirm={handleSubmitExam}
            okText="OK"
            cancelText="Hủy"
          >
            <Button
              className="w-full flex justify-center h-12 mb-3 text-lg"
              type="primary"
            >
              <CheckSquareOutlined />
              Kết thúc làm bài
            </Button>
          </Popconfirm>
          <Popconfirm
            placement="top"
            title="Bạn có muốn hủy làm bài không?"
            onConfirm={() => navigate("/")}
            okText="OK"
            cancelText="Hủy"
          >
            <Button
              className="w-full h-12 flex justify-center text-lg"
              type="warning"
            >
              <HomeOutlined />
              Trở về trang chủ
            </Button>
          </Popconfirm>
        </div>
      </div>
    </div>
  );
};

export default DoExam;
