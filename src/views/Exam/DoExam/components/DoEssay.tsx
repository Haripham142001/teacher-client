import { Input } from "antd";
import FileUploader from "components/UI/FileUploader";
import React from "react";
import { ExamQuestions } from "services/model";

type Props = {
  questions: Partial<ExamQuestions>;
  onChange?: (value: any, field: string) => void;
  type?: string;
};

const DoEssay = (props: Props) => {
  return (
    <div>
      {props?.questions.advance?.allowContent && (
        <>
          {props.type !== "view" && <label htmlFor="">Nhập đáp án</label>}
          <Input.TextArea
            className="my-2"
            value={props.questions.value?.result}
            onChange={(e) => {
              props?.onChange?.(e.target.value, "result");
            }}
          />
        </>
      )}
      {props.type !== "view" && props?.questions.advance?.allowFile && (
        <FileUploader
          onChange={(e) => props?.onChange?.(e.url, "attachment")}
          value={props.questions.value?.attachment}
        />
      )}
    </div>
  );
};

export default DoEssay;
