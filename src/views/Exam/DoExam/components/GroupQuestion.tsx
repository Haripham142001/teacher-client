import { Radio } from "antd";
import { ExamQuestions } from "services/model";

type Props = {
  questions: Partial<ExamQuestions>;
  onChange?: (value: any) => void;
  type?: string;
};

const GroupQuestion = (props: Props) => {
  const handleChoose = (value: any, field: string) => {
    let _value = Object.assign({}, props.questions?.value);
    _value[field] = value;
    props?.onChange?.(_value);
  };

  return (
    <div>
      {props.questions.answers?.map((ans, index) => {
        let isCorrect = !props?.questions?.missArr?.includes(ans.key);
        return (
          <div key={index}>
            <div className="group-question">
              <b className={`${isCorrect ? "text-green-500" : "text-red-500"}`}>
                {ans.label}: {ans?.content}
              </b>
              <br />
              <Radio.Group
                options={ans.options?.map((opt) => {
                  return {
                    label: (
                      <div className="h-10 flex items-center">{opt.label}</div>
                    ),
                    value: opt.key,
                  };
                })}
                value={props.questions?.value?.[ans.key]}
                onChange={(e) => handleChoose(e.target.value, ans.key)}
              />
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default GroupQuestion;
