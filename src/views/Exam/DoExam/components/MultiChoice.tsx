import { Checkbox } from "antd";
import type { CheckboxValueType } from "antd/es/checkbox/Group";
import React from "react";
import { ExamQuestions } from "services/model";

type Props = {
  questions: Partial<ExamQuestions>;
  onChange?: (value: any) => void;
  type?: string;
};

const MultiChoice = (props: Props) => {
  const onChange = (checkedValues: CheckboxValueType[]) => {
    props?.onChange?.(checkedValues);
  };

  console.log(props);
  

  return (
    <div className="do-exam">
      <Checkbox.Group
        value={props.questions.value}
        options={props?.questions?.answers?.map((ans) => ({
          label: ans.label,
          value: ans.key,
        }))}
        onChange={onChange}
        className="flex-col"
      />
    </div>
  );
};

export default MultiChoice;
