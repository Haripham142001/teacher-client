import { Checkbox, Radio } from "antd";
import React from "react";
import { ExamQuestions } from "services/model";

type Props = {
  questions: Partial<ExamQuestions>;
  onChange?: (value: any) => void;
  type?: string;
};

const SingleChoice = (props: Props) => {
  return (
    <div className="mt-3">
      <Radio.Group value={props.questions?.value || null}>
        {props.questions?.answers?.map((ans, index) => {
          let styleText = "";
          if (props.questions.value === ans.key && props.type === "view") {
            if (props.questions.isCorrect) {
              styleText = "text-green-500 font-bold";
            } else {
              styleText = "text-red-500 font-bold";
            }
          }
          return (
            <div key={index} className="mb-2" style={{ minHeight: "50px" }}>
              <Radio
                className={`text-md ${styleText}`}
                value={ans.key}
                onChange={(e) => {
                  props?.onChange?.(e.target.value);
                }}
              >
                {ans.label}
              </Radio>
            </div>
          );
        })}
      </Radio.Group>
    </div>
  );
};

export default SingleChoice;
