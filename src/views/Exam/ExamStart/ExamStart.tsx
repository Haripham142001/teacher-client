import { Card, Image } from "antd";
import React, { useState, useEffect } from "react";
import { useParams, useLocation, useNavigate } from "react-router-dom";
import {
  DataResponse,
  ExamForStudent,
  IExamConfig,
  Lession,
} from "services/model";
import { Col, Row, Statistic } from "antd";
import dayjs from "dayjs";
import Button from "antd-button-color";
import { BulbOutlined, WarningOutlined } from "@ant-design/icons";
import helper from "services/helper";
import apiRequest from "services/api";
import { useSelector } from "react-redux";
import { selectConfig } from "store/slices/configsSlice";

type Props = {};
// Có thời gian sẽ viết lại component này
const { Countdown } = Statistic;

const ExamStart = (props: Props) => {
  const { examId } = useParams();
  const navigate = useNavigate();
  const location = useLocation();
  const state: ExamForStudent = location.state;
  const config = useSelector(selectConfig);

  const [isShow, setIsShow] = useState<boolean>(false);
  const [data, setData] = useState<Partial<IExamConfig>>();
  // const [isCheckExpiry, setIsCheckExpiry] = useState<boolean>(false);

  let lession: Lession = state?.lession;
  let dateExam: string =
    state?.date + " " + config?.["TIME_LESSION"]?.subConfigs?.[lession]?.value;
  const deadline: dayjs.Dayjs = dayjs(dateExam);
  let deadline2 = dayjs(
    new Date(
      new Date(deadline.toString())?.getTime() +
        (data?.advance?.timeAllowLate || data?.time || 1) * 60 * 1000
    )
  );

  useEffect(() => {
    if (!examId || !state) {
      return navigate("/");
    } else {
      fetchData();
    }
  }, [isShow]);

  const fetchData = async () => {
    let res: [DataResponse<IExamConfig>] = await Promise.all([
      apiRequest.getExamConfigBySidForStudent({ sid: examId }),
    ]);
    if (res[0]?.errorCode === 0) {
      setData(res[0].data);
    }
  };

  const handleCreateExam = async () => {
    const result: DataResponse<string> = await apiRequest.createExam({
      sid: examId,
    });
    if (result?.errorCode === 0) {
      navigate("/exam/do", {
        state: result.data,
      });
    }
  };

  return (
    <div className="flex justify-center items-center h-screen">
      <div className="w-1/3 text-center">
        <Card className="w-full h-max relative" title={state.name}>
          <p className="text-center text-3xl font-bold text-blue-400">
            {state.subject?.name} - {state.subject?.idMH}
          </p>
          <Countdown
            value={deadline.toISOString()}
            onFinish={() => setIsShow(true)}
            valueStyle={{
              color: "red",
              fontWeight: "bold",
              textAlign: "center",
              fontSize: "50px",
            }}
          />
          {deadline.toISOString() < new Date().toISOString() && (
            <Countdown
              value={deadline2.toISOString()}
              onFinish={() => navigate(-1)}
              title={
                <span className="text-xs">
                  Thời gian cho phép vào muộn còn lại
                </span>
              }
              valueStyle={{
                color: "red",
                fontWeight: "bold",
                textAlign: "center",
                fontSize: "20px",
              }}
            />
          )}
          {/* <Button
            className="w-full h-10 mt-10 flex justify-center"
            type="info"
            onClick={handleCreateExam}
          >
            <BulbOutlined />
            Bắt đầu làm bài
          </Button> */}
          {(deadline.toISOString() < new Date().toISOString() || isShow) &&
            deadline2.toISOString() > new Date().toISOString() && (
              <Button
                className="w-full h-10 mt-10 flex justify-center"
                type="info"
                onClick={handleCreateExam}
              >
                <BulbOutlined />
                Bắt đầu làm bài
              </Button>
            )}
          {deadline2.toISOString() < new Date().toISOString() && (
            <Button
              className="w-full h-10 mt-10 flex justify-center"
              type="warning"
              disabled
            >
              <WarningOutlined />
              Hết hạn hoặc chưa đến giờ
            </Button>
          )}
        </Card>
        <Image src="" />
      </div>
    </div>
  );
};

export default ExamStart;
