import { Card } from "antd";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import apiRequest from "services/api";
import helper from "services/helper";
import { DataResponse, Question } from "services/model";
import DoEssay from "../DoExam/components/DoEssay";
import MultiChoice from "../DoExam/components/MultiChoice";
import GroupQuestion from "../DoExam/components/GroupQuestion";
import SingleChoice from "../DoExam/components/SingleChoice";

type Props = {};

interface DataExam {
  exam: {
    mssv: string;
    point: Number;
    questions: Question[];
    sid: string;
    timeSubmit: string;
  };
  examconfig: {
    date: string;
    name: string;
    subject_name: string;
  };
}

const user: any = helper.getUser();

const ViewExam = (props: Props) => {
  const location = useLocation();
  let pageId: string = location.search.split("?sid=")[1];
  const navigate = useNavigate();

  const [data, setData] = useState<Partial<DataExam>>({});

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const result: DataResponse<DataExam> = await apiRequest.getExamByStudent({
      sid: pageId,
    });
    if (result.errorCode !== 0) {
      navigate(-1);
    } else {
      setData(result.data);
    }
  };

  return (
    <div className="p-6">
      <center>
        <b className="text-3xl text-blue-400">{data?.examconfig?.name}</b>
      </center>
      <table className="w-1/4 mx-auto mb-3">
        <tbody>
          <tr>
            <td className="w-40">Họ và tên:</td>
            <td className="text-red-500 font-bold">{user.name}</td>
          </tr>
          <tr>
            <td>Mã số sinh viên:</td>
            <td className="text-red-500 font-bold">{user.mssv}</td>
          </tr>
          <tr>
            <td>Môn thi:</td>
            <td className="text-red-500 font-bold">
              {data.examconfig?.subject_name}
            </td>
          </tr>
          <tr>
            <td>Điểm:</td>
            <td className="text-red-500 font-bold">
              {data?.exam?.point + "" || 0}
            </td>
          </tr>
        </tbody>
      </table>
      {data.exam?.questions?.map((qs, index) => {
        let Question;
        switch (qs?.type) {
          case "Tự luận":
            Question = <DoEssay questions={qs || {}} type="view" />;
            break;
          case "Nhiều phương án":
            Question = <MultiChoice questions={qs || {}} type="view" />;
            break;
          case "Nhóm câu hỏi":
            Question = <GroupQuestion questions={qs || {}} type="view" />;
            break;
          default:
            Question = <SingleChoice questions={qs || {}} type="view" />;
            break;
        }

        return (
          <Card
            title={
              <div className="flex justify-between">
                <span>Câu hỏi thứ {index + 1}</span>
                <span>
                  Điểm: {qs?.point?.point} / {qs?.point?.total}
                </span>
              </div>
            }
            className="mb-5"
            key={index}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: (qs?.content || "<div></div>")?.replace(
                  /(<? *script)/gi,
                  "illegalscript"
                ),
              }}
              className="h-full"
            />
            {Question}
          </Card>
        );
      })}
    </div>
  );
};

export default ViewExam;
