import React, { useState, useEffect } from "react";

import "./login.scss";
import { useNavigate } from "react-router-dom";

import { Input } from "antd";
import Button from "antd-button-color";
import ImageBg from "assets/final-exam-results-test-reading-books-words-concept_53876-123721.avif";
import request from "services/request";
import helper from "services/helper";
import { DataResponse, UserInfo, UserTokenInfo } from "services/model";
import local from "services/local";
import apiRequest from "services/api";

type Props = {};

const Login = (props: Props) => {
  const navigate = useNavigate();
  const [data, setData] = useState<{ username: string; password: string }>({
    username: "",
    password: "",
  });

  useEffect(() => {
    let user: UserInfo = local.get("user", {});
    let token: string = local.get("token", "");
    if (user && token) {
      navigate("/", { replace: true });
    }
  }, []);

  const setStateData = (newState: { [field: string]: any }) => {
    setData((preState) => ({ ...preState, ...newState }));
  };

  const handleLogin = async () => {
    if (!data.username || !data.password)
      return helper.toast("error", "Thiếu username hoặc password");

    const res: DataResponse<UserTokenInfo> = await apiRequest["login"]({
      username: data.username,
      password: data.password,
    });
    if (res.errorCode === 0) {
      local.set("token", res.data.token);
      local.set("user", res.data.user);
      navigate("/", { replace: true });
      return helper.toast("success", "Đăng nhập thành công");
    }
  };

  return (
    <div className="login container-fluid bg-white h-screen">
      <div className="header h-16 w-full px-10 flex items-center">
        <b className="text-[40px]">Logo</b>
      </div>
      <div className="container mx-auto grid grid-cols-12 px-16 main">
        <div className="h-full px-6 col-span-12 md:col-span-12 md:px-10 lg:col-span-5">
          <p className="mt-3">
            <b style={{ fontSize: "30px" }}>Login</b>
          </p>
          <p className="mt-6">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </p>
          <p className="mt-3 mb-3">
            <b>Username</b>
          </p>
          <Input
            size="large"
            value={data.username}
            onChange={(e: React.FormEvent<HTMLInputElement>) =>
              setStateData({ username: e.currentTarget.value })
            }
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                handleLogin();
              }
            }}
          />
          <p className="mt-3 mb-3">
            <b>Password</b>
          </p>
          <Input
            size="large"
            value={data.password}
            onChange={(e: React.FormEvent<HTMLInputElement>) =>
              setStateData({ password: e.currentTarget.value })
            }
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                handleLogin();
              }
            }}
            type="password"
          />
          <p className="mt-3 text-right cursor-pointer hover:text-blue-400 hover:underline">
            Forget Password?
          </p>
          <button
            className="w-full mt-3 bg-[#3366ff] h-14 rounded-lg hover:bg-[#4d7aff] hover:text-white"
            onClick={handleLogin}
          >
            <b>Login</b>
          </button>
        </div>
        <div
          className="bg-cover bg-no-repeat rounded-xl hidden md:hidden lg:col-span-7 lg:block"
          style={{ backgroundImage: `url(${ImageBg})` }}
        ></div>
      </div>
    </div>
  );
};

export default Login;
