import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      components: "/src/components",
      store: "/src/store",
      views: "/src/views",
      scss: "/src/scss",
      layout: "/src/layout",
      features: "/src/features",
      constant: "/src/constant",
      services: "/src/services",
      assets: "/src/assets",
    },
  },
});
